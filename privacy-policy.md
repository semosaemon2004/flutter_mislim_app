# Privacy Policy for سبيل المسلم

## Introduction
This Privacy Policy outlines how سبيل المسلم, developed by Waseem Ashmar, collects, uses, and protects user data. It is intended for use as is.

## Collection and Use of Information
For a better experience while using سبيل المسلم, personal information may be required, which will be stored on your device.

## Log Data
In case of an error in سبيل المسلم, data like IP address and device information may be collected.

## Cookies
Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.

## Service Providers
Third-party services may be used for facilitating our Service. They have access to user personal information but are obligated not to disclose it further.

## Security
We aim to protect your personal information but cannot guarantee its absolute security.

## Links to Other Sites
This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

## Children’s Privacy
I do not knowingly collect personally identifiable information from children. I encourage all children to never submit any personally identifiable information through the Application and/or Services. I encourage parents and legal guardians to monitor their children's Internet usage and to help enforce this Policy by instructing their children never to provide personally identifiable information through the Application and/or Services without their permission. If you have reason to believe that a child has provided personally identifiable information to us through the Application and/or Services, please contact us. You must also be at least 16 years of age to consent to the processing of your personally identifiable information in your country (in some countries we may allow your parent or guardian to do so on your behalf).

## Changes to This Privacy Policy
Policy updates will be posted on this page. This policy is effective as of 2024-03-06.

## Contact Us
For questions, contact [mwaflutterdeveloper@gmail.com].
