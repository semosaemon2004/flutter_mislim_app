// import 'package:flutter/material.dart';
// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:my_islamic_app/views/Hadith/hadith_screen.dart';
// import 'package:my_islamic_app/views/alsirahNabawia/alsirah_alnabawia_screen.dart';
// import 'package:my_islamic_app/views/azkar/azkar_screen.dart';
// import 'package:my_islamic_app/views/quran/quran_screen.dart';
// import 'package:smooth_page_indicator/smooth_page_indicator.dart';
// import 'package:intl/intl.dart';
// import 'dart:ui';

// import 'views/qiblah/qiblah_screen.dart';
// import 'views/tasbih/tasbeeh_screen.dart';

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState creaHomeScreenate() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   ScrollController _scrollController = ScrollController();
//   double _blurSigma = 0.0;
//   int _current = 0;

//   final List<String> verses = [
//     'وَاذْكُرُوا اللَّهَ كَثِيرًا لَعَلَّكُمْ تُفْلِحُونَ',
//     'فَاذْكُرُونِي أَذْكُرْكُمْ وَاشْكُرُوا لِي وَلَا تَكْفُرُونِ',
//     'وَإِذَا سَأَلَكَ عِبَادِي عَنِّي فَإِنِّي قَرِيبٌ',
//   ];

//   @override
//   void initState() {
//     super.initState();
//     _scrollController.addListener(() {
//       setState(() {
//         _blurSigma = _scrollController.offset > 0 ? 10.0 : 0.0;
//       });
//     });
//   }

//   @override
//   void dispose() {
//     _scrollController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       extendBodyBehindAppBar: true,
//       body: Stack(
//         children: [
//           SingleChildScrollView(
//             controller: _scrollController,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.stretch,
//               children: [
//                 _buildPrayerTimeContainer(),
//                 _buildGridViewContainer(context),
//                 _buildVerseContainer(),
//                 SizedBox(height: 10),
//                 _buildMessageContainer(),
//                 SizedBox(height: 10),
//               ],
//             ),
//           ),
//           Positioned(
//             top: 0,
//             left: 0,
//             right: 0,
//             child: ClipRect(
//               child: BackdropFilter(
//                 filter:
//                     ImageFilter.blur(sigmaX: _blurSigma, sigmaY: _blurSigma),
//                 child: AppBar(
//                   actions: [
//                     IconButton(
//                       icon: Icon(Icons.notification_add),
//                       color: Colors.white,
//                       onPressed: () {},
//                     ),
//                     Spacer(),
//                     IconButton(
//                       icon: Icon(Icons.menu),
//                       color: Colors.white,
//                       onPressed: () {},
//                     ),
//                   ],
//                   backgroundColor: Colors.transparent,
//                   elevation: 0,
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   Widget _buildPrayerTimeContainer() {
//     return Container(
//       padding: EdgeInsets.all(16),
//       decoration: BoxDecoration(
//         color: Color(0xff0463ca),
//         image: DecorationImage(
//           image: AssetImage('assets/images/background5.png'),
//           fit: BoxFit.cover,
//           // colorFilter: ColorFilter.mode(
//           //   Colors.black.withOpacity(0.4),
//           //   BlendMode.overlay,
//           // ),
//         ),
//       ),
//       child: Column(
//         children: [
//           Padding(
//             padding: const EdgeInsets.only(top: 80.0), // Add top padding here
//             child: Text(
//               'الصلاةالعشاء\n 07:44',
//               style: TextStyle(
//                   fontSize: 24,
//                   fontWeight: FontWeight.bold,
//                   color: Color(0xffcee3f4)),
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(top: 16.0), // Add top padding here
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Text(
//                   'باقي على الصلاة\n 00:30:00',
//                   style: TextStyle(fontSize: 18, color: Color(0xffcee3f4)),
//                 ),
//                 Text(
//                   'تاريخ اليوم\n ${DateFormat('yyyy-MM-dd').format(DateTime.now())}',
//                   style: TextStyle(fontSize: 18, color: Color(0xffcee3f4)),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   Widget _buildGridViewContainer(BuildContext context) {
//   final List<Map<String, dynamic>> items = [
//     {
//       'icon': "assets/images/quran.png",
//       'label': 'القرآن',
//       'onTap': () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => QuranScreen()),
//         );
//       }
//     },
//     {
//       'icon': "assets/images/hadith.png",
//       'label': 'الحديث',
//       'onTap': () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => HadithScreen()),
//         );
//       }
//     },
//     {
//       'icon': "assets/images/muhammad.png",
//       'label': 'مواقيت الصلاة',
//       'onTap': () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => SirahNabawiaScreen()),
//         );
//       }
//     },
//     {
//       'icon': "assets/images/kaaba.png",
//       'label': 'القبلة',
//       'onTap': () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => QiblahScreen()),
//         );
//       }
//     },
//     {
//       'icon': "assets/images/dua.png",
//       'label': 'الأذكار',
//       'onTap': () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => AzkarScreen()),
//         );
//       },
//     },
//     {
//       'icon': "assets/images/tasbih.png",
//       'label': 'التسبيح',
//       'onTap': () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => TasbeehCounterScreen()),
//         );
//       }
//     },
//   ];

//   return Container(
//     color: Colors.white,
//     child: GridView.builder(
//       shrinkWrap: true,
//       physics: NeverScrollableScrollPhysics(),
//       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//         crossAxisCount: 3,
//         crossAxisSpacing: 15,
//         mainAxisSpacing: 15,
//       ),
//       itemCount: items.length,
//       itemBuilder: (context, index) {
//         return _buildGridItem(
//           items[index]['icon'],
//           items[index]['label'],
//           items[index]['onTap'],
//         );
//       },
//     ),
//   );
// }

// Widget _buildGridItem(dynamic icon, String label, void Function() onTap) {
//   return InkWell(
//     onTap: onTap,
//     child: Column(
//       children: [
//         Container(
//           width: 60,
//           height: 60,
//           decoration: BoxDecoration(
//             shape: BoxShape.circle,
//             color: Color(0xffb0d6f5), // Icon background color
//           ),
//           child: Center(
//             child: 
//                  Image.asset(
//                     icon,
//                     width: 30,
//                     height: 30,
//                     //color: Colors.teal[700], // Icon color
//                   )
                
//           ),
//         ),
//         SizedBox(height: 8),
//         Text(
//           label,
//           textAlign: TextAlign.center,
//           style: TextStyle(
//             fontSize: 14,
//             fontWeight: FontWeight.bold,
//             color: Colors.teal[700], // Text color
//           ),
//         ),
//       ],
//     ),
//   );
// }

//   Widget _buildVerseContainer() {
//     return Container(
//       color: Color(0xffcee3f4),
//       padding: EdgeInsets.all(16),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Row(
//                 children: [
//                   IconButton(
//                     onPressed: () {},
//                     icon: Icon(Icons.share),
//                     color: Color(0xff0463ca),
//                   ),
//                   IconButton(
//                     onPressed: () {},
//                     icon: Icon(Icons.copy),
//                     color: Color(0xff0463ca),
//                   ),
//                 ],
//               ),
//               Text(
//                 'آية وعبرة',
//                 style: TextStyle(
//                     fontSize: 24,
//                     fontWeight: FontWeight.bold,
//                     color: Color(0xff0463ca)),
//               ),
//             ],
//           ),
//           SizedBox(height: 25),
//           CarouselSlider.builder(
//             itemCount: verses.length,
//             options: CarouselOptions(
//               height: 100,
//               autoPlay: true,
//               enlargeCenterPage: true,
//               viewportFraction: 1.0,
//               autoPlayInterval: Duration(seconds: 35),
//               autoPlayAnimationDuration: Duration(milliseconds: 800),
//               autoPlayCurve: Curves.fastOutSlowIn,
//               pauseAutoPlayOnTouch: true,
//               scrollDirection: Axis.horizontal,
//               onPageChanged: (index, reason) {
//                 setState(() {
//                   _current = index;
//                 });
//               },
//             ),
//             itemBuilder: (context, index, realIndex) {
//               return Column(
//                 children: [
//                   Expanded(
//                     child: Text(
//                       verses[index],
//                       style: TextStyle(
//                           fontSize: 18,
//                           fontStyle: FontStyle.italic,
//                           color: Colors.orange[800]),
//                       textAlign: TextAlign.center,
//                     ),
//                   ),
//                   Expanded(
//                     child: Text(
//                       " شرح ايه",
//                       style: TextStyle(
//                           fontSize: 18,
//                           fontStyle: FontStyle.italic,
//                           color: Colors.white),
//                       textAlign: TextAlign.center,
//                     ),
//                   ),
//                 ],
//               );
//             },
//           ),
//           Center(
//             child: SmoothPageIndicator(
//               controller: PageController(initialPage: _current),
//               count: verses.length,
//               effect: WormEffect(
//                 activeDotColor: Colors.orange,
//                 dotHeight: 8.0,
//                 dotWidth: 8.0,
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   Widget _buildMessageContainer() {
//     return Stack(
//       children: [
//         Container(
//           height: 200,
//           width: 120,
//           decoration: BoxDecoration(
//             color: Color(0xff0463ca),
//             borderRadius: BorderRadius.only(
//               topRight: Radius.circular(50),
//               bottomRight: Radius.circular(50),
//             ),
//           ),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(right: 40),
//                 child: CircleAvatar(
//                   child: IconButton(
//                     onPressed: () {},
//                     icon: Icon(Icons.copy),
//                   ),
//                 ),
//               ),
//               SizedBox(height: 15),
//               Padding(
//                 padding: const EdgeInsets.only(right: 40),
//                 child: CircleAvatar(
//                   child: IconButton(
//                     onPressed: () {},
//                     icon: Icon(Icons.share),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//         Positioned(
//           left: 90,
//           top: 20,
//           bottom: 20,
//           child: Container(
//             width: 300,
//             padding: EdgeInsets.all(16),
//             decoration: BoxDecoration(
//               color: Color(0xffcee3f4),
//               borderRadius: BorderRadius.only(
//                 topLeft: Radius.circular(30),
//                 bottomLeft: Radius.circular(30),
//               ),
//             ),
//             child: Column(
//               children: [
//                 Text(
//                   'رسالتك اليوم',
//                   style: TextStyle(
//                       fontSize: 24,
//                       fontWeight: FontWeight.bold,
//                       color: Colors.purple[800]),
//                 ),
//                 SizedBox(height: 10),
//                 Text(
//                   'اجعل يومك مليئًا بالخير والعمل الصالح',
//                   style: TextStyle(fontSize: 18, color: Colors.purple[800]),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }
