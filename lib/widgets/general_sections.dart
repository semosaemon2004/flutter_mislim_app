import 'package:flutter/material.dart';
import 'package:my_islamic_app/utils/colors.dart';

class GeneralSections extends StatelessWidget {
  final String svgIcon;
  final String title;
  final VoidCallback onTap;

  const GeneralSections({
    Key? key,
    required this.svgIcon,
    required this.title,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        // Calculate the size for the icon dynamically
        double iconSize = constraints.maxHeight * 0.28;

        return InkWell(
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
              color: AppColors.lightAccent,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  svgIcon,
                  height: iconSize, // Use the dynamic size for the icon
                ),
                const SizedBox(height: 8),
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Cairo",
                    fontWeight: FontWeight.bold,
                    fontSize: constraints.maxHeight * 0.1, // Dynamic text size
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
