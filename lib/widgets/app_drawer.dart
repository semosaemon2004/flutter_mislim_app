import 'package:flutter/material.dart';
import 'package:flutter_slider_drawer/flutter_slider_drawer.dart';
import 'package:animations/animations.dart';
import '../utils/colors.dart';

class AppDrawer extends StatelessWidget {
  final Function(int) onPageChanged;
  final int currentIndex;
  final GlobalKey<SliderDrawerState> drawerKey;

  const AppDrawer({
    Key? key,
    required this.onPageChanged,
    required this.currentIndex,
    required this.drawerKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(1, 0), // Start from the right
        end: Offset.zero, // End at the center
      ).animate(CurvedAnimation(
        parent: ModalRoute.of(context)!.animation!,
        curve: Curves.easeOutCubic,
      )),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.85,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              AppColors.lightPrimary,
              AppColors.lightPrimary.withOpacity(0.9),
            ],
          ),
          borderRadius: const BorderRadius.horizontal(left: Radius.circular(20)),
        ),
        child: SafeArea(
          child: Column(
            children: [
              const SizedBox(height: 20),
              // Header
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    Hero(
                      tag: 'app_logo',
                      child: Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white, width: 2),
                          image: const DecorationImage(
                            image: AssetImage('assets/images/logo.png'),
                            fit: BoxFit.cover,
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              blurRadius: 10,
                              offset: const Offset(0, 5),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 16),
                    const Text(
                      'تطبيق الإسلامي',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontFamily: 'Cairo',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Text(
                      'رحلتك نحو السكينة',
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 16,
                        fontFamily: 'Cairo',
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(color: Colors.white24),
              const SizedBox(height: 10),
              // Menu Items
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      children: [
                        _buildDrawerItem(
                          icon: Icons.menu_book,
                          title: 'القرآن الكريم',
                          index: 0,
                          context: context,
                        ),
                        _buildDrawerItem(
                          icon: Icons.format_quote,
                          title: 'الأحاديث النبوية',
                          index: 1,
                          context: context,
                        ),
                        _buildDrawerItem(
                          icon: Icons.brightness_4,
                          title: 'الأذكار',
                          index: 2,
                          context: context,
                        ),
                        _buildDrawerItem(
                          icon: Icons.mosque,
                          title: 'أوقات الصلاة',
                          index: 3,
                          context: context,
                        ),
                        _buildDrawerItem(
                          icon: Icons.compass_calibration,
                          title: 'القبلة',
                          index: 4,
                          context: context,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              // Bottom Items
              const Divider(color: Colors.white24),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Column(
                  children: [
                    _buildDrawerItem(
                      icon: Icons.settings,
                      title: 'الإعدادات',
                      index: 5,
                      context: context,
                    ),
                    _buildDrawerItem(
                      icon: Icons.info,
                      title: 'عن التطبيق',
                      index: 6,
                      context: context,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDrawerItem({
    required IconData icon,
    required String title,
    required int index,
    required BuildContext context,
  }) {
    final isSelected = currentIndex == index;
    return TweenAnimationBuilder<double>(
      tween: Tween(begin: 0.0, end: 1.0),
      duration: Duration(milliseconds: 200 + (index * 100)),
      builder: (context, value, child) {
        return Transform.translate(
          offset: Offset(50 * (1 - value), 0),
          child: Opacity(
            opacity: value,
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 4),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    onPageChanged(index);
                    Navigator.pop(context);
                  },
                  borderRadius: BorderRadius.circular(10),
                  child: AnimatedContainer(
                    duration: const Duration(milliseconds: 300),
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                    decoration: BoxDecoration(
                      color: isSelected ? Colors.white.withOpacity(0.1) : Colors.transparent,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: [
                        Icon(
                          icon,
                          color: isSelected ? Colors.white : Colors.white70,
                          size: 26,
                        ),
                        const SizedBox(width: 16),
                        Text(
                          title,
                          style: TextStyle(
                            color: isSelected ? Colors.white : Colors.white70,
                            fontSize: 16,
                            fontFamily: 'Cairo',
                            fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}