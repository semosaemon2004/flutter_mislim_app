import 'package:flutter/material.dart';

class FontSizeMenu extends StatelessWidget {
  final Function(double) onSelected;
  final double currentSize;

  const FontSizeMenu({
    Key? key,
    required this.onSelected,
    required this.currentSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<double>(
      icon: Icon(Icons.text_fields, color: Colors.white),
      initialValue: currentSize,
      onSelected: onSelected,
      itemBuilder: (context) => [
        PopupMenuItem(
          value: 22.0,
          child: Text('صغير', style: TextStyle(fontFamily: "Cairo")),
        ),
        PopupMenuItem(
          value: 27.0,
          child: Text('متوسط', style: TextStyle(fontFamily: "Cairo")),
        ),
        PopupMenuItem(
          value: 35.0,
          child: Text('كبير', style: TextStyle(fontFamily: "Cairo")),
        ),
      ],
    );
  }
}
