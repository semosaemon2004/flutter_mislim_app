class SirahNabawiaContent {
  final int id;
  final String title;
  final String content;
  final String? reference;
  final String? date;
  final String? location;
  final List<String>? keyEvents;

  SirahNabawiaContent({
    required this.id,
    required this.title,
    required this.content,
    this.reference,
    this.date,
    this.location,
    this.keyEvents,
  });

  factory SirahNabawiaContent.fromJson(Map<String, dynamic> json) {
    return SirahNabawiaContent(
      id: json['id'] as int,
      title: json['title'] as String,
      content: json['content'] as String,
      reference: json['reference'] as String?,
      date: json['date'] as String?,
      location: json['location'] as String?,
      keyEvents: json['keyEvents'] != null
          ? List<String>.from(json['keyEvents'] as List)
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'content': content,
      'reference': reference,
      'date': date,
      'location': location,
      'keyEvents': keyEvents,
    };
  }
}