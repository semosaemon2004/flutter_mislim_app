class HadithContent {
  int? hadithId;
  String content;

  HadithContent({this.hadithId, required this.content});

  factory HadithContent.fromJson(Map<String, dynamic> json) {
    return HadithContent(
      hadithId: int.tryParse(json['hadith_id'] as String? ?? ''),
      content: json['content'] as String? ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'hadith_id': hadithId,
      'content': content,
    };
  }
}
