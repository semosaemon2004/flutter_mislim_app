import 'package:adhan/adhan.dart';

class SerializablePrayerTimes {
  final Coordinates coordinates;
  final CalculationParameters params;
  final DateTime fajr;
  final DateTime sunrise;
  final DateTime dhuhr;
  final DateTime asr;
  final DateTime maghrib;
  final DateTime isha;

  SerializablePrayerTimes({
    required this.coordinates,
    required this.params,
    required this.fajr,
    required this.sunrise,
    required this.dhuhr,
    required this.asr,
    required this.maghrib,
    required this.isha,
  });

  factory SerializablePrayerTimes.fromPrayerTimes(PrayerTimes prayerTimes) {
    return SerializablePrayerTimes(
      coordinates: prayerTimes.coordinates,
      params: prayerTimes.calculationParameters,
      fajr: prayerTimes.fajr,
      sunrise: prayerTimes.sunrise,
      dhuhr: prayerTimes.dhuhr,
      asr: prayerTimes.asr,
      maghrib: prayerTimes.maghrib,
      isha: prayerTimes.isha,
    );
  }

  PrayerTimes toPrayerTimes() {
    final date = DateComponents.from(DateTime.now());
    return PrayerTimes(coordinates, date, params);
  }

  Map<String, dynamic> toJson() => {
        'latitude': coordinates.latitude,
        'longitude': coordinates.longitude,
        'methodIndex':
            params.method?.index ?? 3, // Default to muslim_world_league if null
        'madhab': params.madhab?.index ?? 0, // Default to shafi if null
      };

  factory SerializablePrayerTimes.fromJson(Map<String, dynamic> json) {
    final coordinates = Coordinates(
      double.parse(json['latitude'].toString()),
      double.parse(json['longitude'].toString()),
    );

    final methodIndex =
        json['methodIndex'] as int? ?? 3; // Default to muslim_world_league
    final madhabIndex = json['madhab'] as int? ?? 0; // Default to shafi

    final params = CalculationMethod.values[methodIndex].getParameters();
    params.madhab = Madhab.values[madhabIndex];

    // Create a new PrayerTimes instance with current date
    final prayerTimes = PrayerTimes(
      coordinates,
      DateComponents.from(DateTime.now()),
      params,
    );

    // Return a new SerializablePrayerTimes using the calculated prayer times
    return SerializablePrayerTimes(
      coordinates: coordinates,
      params: params,
      fajr: prayerTimes.fajr,
      sunrise: prayerTimes.sunrise,
      dhuhr: prayerTimes.dhuhr,
      asr: prayerTimes.asr,
      maghrib: prayerTimes.maghrib,
      isha: prayerTimes.isha,
    );
  }
}
