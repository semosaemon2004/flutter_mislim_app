class SurahContent {
  final String index;
  final String name;
  final Map<String, String> verses;
  final int count;
  final List<Juz> juz;

  SurahContent({
    required this.index,
    required this.name,
    required this.verses,
    required this.count,
    required this.juz,
  });

  factory SurahContent.fromJson(Map<String, dynamic> json) {
    var versesMap = json['verse'] as Map? ?? {};
    Map<String, String> verses = {};
    for (var key in versesMap.keys) {
      var stringKey = key.toString();
      // Use a null check instead of a cast, to avoid the error
      var value = versesMap[key];
      verses[stringKey] = value is String ? value : '';
    }

    return SurahContent(
      index: json['index'] as String? ?? '',
      name: json['name'] as String? ?? '',
      verses: verses,
      count: json['count'] as int? ?? 0,
      juz: (json['juz'] as List?)
              ?.map((juzJson) => Juz.fromJson(juzJson as Map<String, dynamic>))
              .toList() ??
          [],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'index': index,
      'name': name,
      'verse': verses,
      'count': count,
      'juz': juz.map((juzItem) => juzItem.toJson()).toList(),
    };
  }
}

class Juz {
  final String index;
  final VerseRange verseRange;

  Juz({
    required this.index,
    required this.verseRange,
  });

  factory Juz.fromJson(Map<String, dynamic> json) {
    return Juz(
      index: json['index'] as String,
      verseRange: VerseRange.fromJson(json['verse']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'index': index,
      'verse': verseRange.toJson(),
    };
  }
}

class VerseRange {
  final String start;
  final String end;

  VerseRange({
    required this.start,
    required this.end,
  });

  factory VerseRange.fromJson(Map<String, dynamic> json) {
    return VerseRange(
      start: json['start'] as String,
      end: json['end'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'start': start,
      'end': end,
    };
  }
}
