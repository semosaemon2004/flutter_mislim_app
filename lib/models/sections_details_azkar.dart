class SectiosDetailsAzkar {
  int? sectionId;
  String? count;
  String? description;
  String? reference;
  String? content;

  SectiosDetailsAzkar({
    this.sectionId,
    this.count,
    this.description,
    this.content,
    this.reference,
  });

  factory SectiosDetailsAzkar.fromJson(Map<String, dynamic> json) {
    return SectiosDetailsAzkar(
      sectionId: json['section_id'] as int?,
      count: json['count'] as String?,
      description: json['description'] as String?,
      content: json['content'] as String?,
      reference: json['reference'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'section_id': sectionId,
      'count': count,
      'description': description,
      'content': content,
      'reference': reference,
    };
  }
}
