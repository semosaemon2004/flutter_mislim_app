class Pages {
  final String index;
  final PagesSurah start;
  final PagesSurah end;

  Pages({required this.index, required this.start, required this.end});

  factory Pages.fromJson(Map<String, dynamic> json) {
    return Pages(
      index: json['index'],
      start: PagesSurah.fromJson(json['start']),
      end: PagesSurah.fromJson(json['end']),
    );
  }
}

class PagesSurah {
  final String index;
  final String verse;
  final String name;
  final String nameAr;

  PagesSurah({required this.index, required this.verse, required this.name, required this.nameAr});

  factory PagesSurah.fromJson(Map<String, dynamic> json) {
    return PagesSurah(
      index: json['index'],
      verse: json['verse'],
      name: json['name'],
      nameAr: json['nameAr'],
    );
  }
}
