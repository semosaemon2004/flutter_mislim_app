// models/juz.dart
class Juz {
  final String index;
  final JuzSurah start;
  final JuzSurah end;
  final int startPage;
  final int endPage;

  Juz({
    required this.index,
    required this.start,
    required this.end,
    required this.startPage,
    required this.endPage,
  });

  factory Juz.fromJson(Map<String, dynamic> json) {
    return Juz(
      index: json['index'] as String,
      start: JuzSurah.fromJson(json['start'] as Map<String, dynamic>),
      end: JuzSurah.fromJson(json['end'] as Map<String, dynamic>),
      startPage: json['startPage'] != null ? json['startPage'] as int : 0, // Defaulting to 0 if null
      endPage: json['endPage'] != null ? json['endPage'] as int : 0,       // Defaulting to 0 if null
    );
  }
}

class JuzSurah {
  final String index;
  final String verse;
  final String name;
  final String nameAr;

  JuzSurah({
    required this.index,
    required this.verse,
    required this.name,
    required this.nameAr,
  });

  factory JuzSurah.fromJson(Map<String, dynamic> json) {
    return JuzSurah(
      index: json['index'] as String,
      verse: json['verse'] as String,
      name: json['name'] as String,
      nameAr: json['nameAr'] as String,
    );
  }
}
