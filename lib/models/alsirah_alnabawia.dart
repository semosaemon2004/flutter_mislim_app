class SirahNabawia {
  int id;
  String name;

  SirahNabawia({required this.id, required this.name});

  factory SirahNabawia.fromJson(Map<String, dynamic> json) {
    return SirahNabawia(
      id: json['id'] as int,
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
    };
  }
}
