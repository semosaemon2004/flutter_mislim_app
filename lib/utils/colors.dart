// lib/utils/colors.dart

import 'package:flutter/material.dart';

class AppColors {
  // Light Theme Colors
  static const Color lightPrimary = Color(0xFF395886);
  static const Color lightAccent = Color(0xFF8AAEE0);
  static const Color lightBackground = Color(0xFFF0F3FA);

  // Dark Theme Colors
  static const Color darkPrimary = Color(0xFF006DA4);
  static const Color darkAccent = Color(0xFF004D74);
  static const Color darkBackground = Color(0xFF032030);
}
