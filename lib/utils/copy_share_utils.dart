import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share_plus/share_plus.dart';

void copyToClipboard(BuildContext context, String text) {
  final data = ClipboardData(text: text);
  Clipboard.setData(data);
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text('تم نسخ الرسالة', textAlign: TextAlign.center),
      behavior: SnackBarBehavior.floating,
      width: 200, // Set the width of the SnackBar
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
    ),
  );
}

void shareText(BuildContext context, String text) {
  final additionalText = '\n\nتم النشر بواسطة تطبيق سبيل المسلم \nحمل التطبيق من Google Play من هنا 👇 \nhttps://play.google.com/store/apps/details?id=www.waseem.muslim.app';
  final completeText = '$text$additionalText';

  Share.share(completeText);
}
