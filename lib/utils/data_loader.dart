import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class DataLoader {
  final String url;
  final String cacheKey;

  DataLoader({required this.url, required this.cacheKey});

  Future<List<dynamic>> fetchData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? cachedData = prefs.getString(cacheKey);
    String? lastUpdate = prefs.getString('$cacheKey-lastUpdate');

    DateTime now = DateTime.now();
    if (cachedData != null && lastUpdate != null) {
      DateTime lastUpdateTime = DateTime.parse(lastUpdate);
      if (lastUpdateTime.day == now.day &&
          lastUpdateTime.month == now.month &&
          lastUpdateTime.year == now.year) {
        return json.decode(cachedData) as List<dynamic>;
      }
    }

    bool isOnline = await _checkConnectivity();
    if (isOnline) {
      final response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        prefs.setString(cacheKey, response.body);
        prefs.setString('$cacheKey-lastUpdate', now.toIso8601String());
        return json.decode(response.body) as List<dynamic>;
      } else {
        throw Exception('Failed to load JSON file');
      }
    } else {
      throw Exception('No internet connection and no cached data available');
    }
  }

  Future<bool> _checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
}
