import 'package:flutter/material.dart';
import 'package:my_islamic_app/test.dart';
import 'package:my_islamic_app/views/home/home_screen.dart';
import 'package:my_islamic_app/views/splash/splash_screen.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:my_islamic_app/widgets/nav_bar.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Muslim App',
      debugShowCheckedModeBanner: false,
      // theme: ThemeData(
      //   primaryColor: AppColors.lightPrimary,
      //   colorScheme: ColorScheme.fromSwatch().copyWith(
      //     secondary: AppColors.lightAccent,
      //     background: AppColors.lightBackground,
      //   ),
      // ),
      // darkTheme: ThemeData(
      //   primaryColor: AppColors.darkPrimary,
      //   colorScheme: ColorScheme.fromSwatch(
      //     brightness: Brightness.dark,
      //   ).copyWith(
      //     secondary: AppColors.darkAccent,
      //     background: AppColors.darkBackground,
      //   ),
      // ),
      home: NavBar(),
    );
  }
}
