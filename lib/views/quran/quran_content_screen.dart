import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../models/surah_content.dart';
import '../../utils/colors.dart';
import '../../widgets/font_size_menu.dart';

class QuranContentScreen extends StatefulWidget {
  final String surahName;
  final String? initialVerse;
  final int surahNumber;

  const QuranContentScreen({
    Key? key,
    required this.surahName,
    required this.surahNumber,
    this.initialVerse,
  }) : super(key: key);

  @override
  State<QuranContentScreen> createState() => _QuranContentScreenState();
}

class _QuranContentScreenState extends State<QuranContentScreen> {
  late Future<SurahContent> surahContentFuture;
  TextEditingController searchController = TextEditingController();
  bool isSearching = false;
  Map<String, String> allVerses = {};
  Map<String, String> filteredVerses = {};
  double _fontSize = 24.0;
  String? lastReadVerse;
  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener =
      ItemPositionsListener.create();
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    surahContentFuture = loadSurahContentData();
    _loadLastReadVerse();
  }

  Future<void> _scrollToVerse(String verseNumber) async {
    if (!mounted) return;

    try {
      // انتظر حتى يتم تحميل البيانات
      while (_isLoading) {
        await Future.delayed(Duration(milliseconds: 100));
        if (!mounted) return;
      }

      // إزالة البادئة 'verse_' إذا كانت موجودة
      String cleanVerseNumber = verseNumber.replaceAll('verse_', '');

      // حساب موقع الآية في القائمة
      int targetIndex = int.parse(cleanVerseNumber) - 1;
      if (widget.surahNumber != 1 && widget.surahNumber != 9) {
        targetIndex++; // إضافة 1 للبسملة
      }

      // التمرير إلى الآية المحددة
      if (itemScrollController.isAttached) {
        await itemScrollController.scrollTo(
          index: targetIndex,
          duration: Duration(milliseconds: 500),
          curve: Curves.easeInOut,
          alignment: 0.1, // موقع العنصر في الشاشة (0 = أعلى، 1 = أسفل)
        );

        setState(() {
          lastReadVerse = cleanVerseNumber;
        });
      }
    } catch (e) {
      print('Error scrolling to verse: $e');
    }
  }

  Future<SurahContent> loadSurahContentData() async {
    setState(() => _isLoading = true);

    try {
      String jsonString =
          await rootBundle.loadString('assets/data/quran_db.json');
      List<dynamic> jsonResponse = json.decode(jsonString) as List;
      final surahData = jsonResponse.firstWhere(
        (surah) => surah['name'] == widget.surahName,
        orElse: () => {'verse': {}},
      );
      SurahContent content =
          SurahContent.fromJson(Map<String, dynamic>.from(surahData));
      allVerses = content.verses;
      filteredVerses = Map.from(allVerses);

      setState(() {
        _isLoading = false;
      });

      // تأخير التمرير حتى يتم تحميل البيانات
      if (widget.initialVerse != null) {
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          await Future.delayed(Duration(milliseconds: 300));
          _scrollToVerse(widget.initialVerse!);
        });
      }

      return content;
    } catch (e) {
      print('Error loading surah data: $e');
      setState(() => _isLoading = false);
      rethrow;
    }
  }

  void filterVerses(String query) {
    setState(() {
      if (query.isEmpty) {
        filteredVerses = Map.from(allVerses);
      } else {
        // Split the search query into individual words
        List<String> searchWords = query.trim().split(' ');

        filteredVerses = Map.fromEntries(
          allVerses.entries.where((entry) {
            String verseText = entry.value;

            // Check if all search words are present in the verse
            bool containsAllWords = searchWords.every((word) {
              return verseText.contains(word);
            });

            return containsAllWords;
          }),
        );
      }
    });
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.grey[50],
        appBar: AppBar(
          elevation: 0,
          backgroundColor: AppColors.lightPrimary,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.pop(context),
          ),
          title: !isSearching
              ? Text(
                  "سورة ${widget.surahName}",
                  style: TextStyle(
                    fontSize: 24,
                    fontFamily: "Cairo",
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
              : TextField(
                  controller: searchController,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    hintText: 'ابحث عن كلمة أو حرف...',
                    hintStyle: TextStyle(
                      color: Colors.white70,
                      fontFamily: "Cairo",
                    ),
                    border: InputBorder.none,
                  ),
                  onChanged: filterVerses,
                ),
          actions: [
            IconButton(
              icon: Icon(isSearching ? Icons.close : Icons.search),
              color: Colors.white,
              onPressed: () {
                setState(() {
                  if (isSearching) {
                    isSearching = false;
                    searchController.clear();
                    filterVerses('');
                  } else {
                    isSearching = true;
                  }
                });
              },
            ),
            FontSizeMenu(
              currentSize: _fontSize,
              onSelected: (double size) {
                setState(() => _fontSize = size);
              },
            ),
          ],
        ),
        body: FutureBuilder<SurahContent>(
          future: surahContentFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(AppColors.lightPrimary),
                ),
              );
            }
            if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.error_outline, size: 48, color: Colors.red),
                    SizedBox(height: 16),
                    Text(
                      'حدث خطأ في تحميل السورة',
                      style: TextStyle(
                        fontFamily: "Cairo",
                        fontSize: 18,
                        color: Colors.red,
                      ),
                    ),
                  ],
                ),
              );
            }

            bool shouldPrependBismillah =
                widget.surahName != 'الفاتحة' && widget.surahName != 'التوبة';

            return AnimationLimiter(
              child: ScrollablePositionedList.builder(
                itemScrollController: itemScrollController,
                itemPositionsListener: itemPositionsListener,
                padding: EdgeInsets.all(16),
                itemCount:
                    filteredVerses.length + (shouldPrependBismillah ? 1 : 0),
                itemBuilder: (context, index) {
                  if (shouldPrependBismillah && index == 0) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 375),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 20),
                            padding: EdgeInsets.symmetric(vertical: 20),
                            decoration: BoxDecoration(
                              color: AppColors.lightPrimary.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Text(
                              'بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ',
                              style: TextStyle(
                                fontSize: _fontSize + 2,
                                fontFamily: "Cairo",
                                color: AppColors.lightPrimary,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    );
                  }

                  final actualIndex =
                      shouldPrependBismillah ? index - 1 : index;
                  final verseNumber =
                      filteredVerses.keys.elementAt(actualIndex);
                  final verseText = filteredVerses[verseNumber]!;
                  final cleanVerseNumber = verseNumber.replaceAll('verse_', '');
                  final isLastRead = lastReadVerse == cleanVerseNumber;

                  return AnimationConfiguration.staggeredList(
                    position: index,
                    duration: const Duration(milliseconds: 375),
                    child: SlideAnimation(
                      verticalOffset: 50.0,
                      child: FadeInAnimation(
                        child: Card(
                          elevation: 2,
                          margin: EdgeInsets.only(bottom: 12),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Container(
                            padding: EdgeInsets.all(16),
                            decoration: BoxDecoration(
                              color: isLastRead
                                  ? AppColors.lightPrimary.withOpacity(0.1)
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  verseText,
                                  style: TextStyle(
                                    fontSize: _fontSize,
                                    height: 2.0,
                                    fontFamily: "quranFont",
                                    letterSpacing: 0.1,
                                    wordSpacing: 1.0,
                                  ),
                                  textAlign: TextAlign.justify,
                                  textDirection: TextDirection.rtl,
                                ),
                                SizedBox(height: 16),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 12,
                                    vertical: 6,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 12,
                                          vertical: 6,
                                        ),
                                        decoration: BoxDecoration(
                                          color: AppColors.lightPrimary
                                              .withOpacity(0.1),
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        child: Text(
                                          'آية $cleanVerseNumber',
                                          style: TextStyle(
                                            color: AppColors.lightPrimary,
                                            fontFamily: "Cairo",
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          if (isLastRead)
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 12,
                                                vertical: 6,
                                              ),
                                              decoration: BoxDecoration(
                                                color: AppColors.lightPrimary
                                                    .withOpacity(0.1),
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.bookmark,
                                                    color:
                                                        AppColors.lightPrimary,
                                                    size: 20,
                                                  ),
                                                  SizedBox(width: 4),
                                                  Text(
                                                    'آخر قراءة',
                                                    style: TextStyle(
                                                      color: AppColors
                                                          .lightPrimary,
                                                      fontFamily: "Cairo",
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          SizedBox(width: 8),
                                          IconButton(
                                            icon: Icon(
                                              isLastRead
                                                  ? Icons.bookmark
                                                  : Icons.bookmark_border,
                                              color: AppColors.lightPrimary,
                                              size: 30,
                                            ),
                                            onPressed: () =>
                                                _saveLastReadVerse(verseNumber),
                                            tooltip: 'حفظ موقع القراءة',
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> _saveLastReadVerse(String verseNumber) async {
    final prefs = await SharedPreferences.getInstance();

    // مسح جميع المواقع المحفوظة السابقة
    Set<String> keys = prefs.getKeys();
    for (String key in keys) {
      if (key.startsWith('lastRead_')) {
        await prefs.remove(key);
      }
    }

    // إزالة البادئة 'verse_' إذا كانت موجودة
    String cleanVerseNumber = verseNumber.replaceAll('verse_', '');

    // حفظ الموقع الجديد
    await prefs.setString('lastRead_${widget.surahName}', cleanVerseNumber);
    await prefs.setString('lastRead_activeSurah', widget.surahName);

    setState(() {
      lastReadVerse = cleanVerseNumber;
    });

    // إرجاع نتيجة الحفظ للشاشة السابقة
    Navigator.pop(context, {
      'surahName': widget.surahName,
      'verseNumber': cleanVerseNumber,
    });

    // إظهار رسالة للمستخدم
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Row(
          textDirection: TextDirection.rtl,
          children: [
            Icon(Icons.bookmark, color: Colors.white),
            SizedBox(width: 8),
            Text(
              'تم حفظ موقع القراءة في سورة ${widget.surahName} - الآية $cleanVerseNumber',
              style: TextStyle(fontFamily: "Cairo"),
            ),
          ],
        ),
        backgroundColor: AppColors.lightPrimary,
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.all(10),
        duration: Duration(seconds: 2),
      ),
    );
  }

  Future<void> _loadLastReadVerse() async {
    final prefs = await SharedPreferences.getInstance();
    String? activeSurah = prefs.getString('lastRead_activeSurah');

    setState(() {
      if (activeSurah == widget.surahName) {
        lastReadVerse = prefs.getString('lastRead_${widget.surahName}');
      } else {
        lastReadVerse = null;
      }
    });
  }
}
