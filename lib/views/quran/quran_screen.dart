import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_islamic_app/models/surah.dart';
import 'package:my_islamic_app/views/quran/quran_content_screen.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuranScreen extends StatefulWidget {
  const QuranScreen({super.key});

  @override
  State<QuranScreen> createState() => _QuranScreenState();
}

class _QuranScreenState extends State<QuranScreen> {
  late Future<List<Surah>> surahsFuture;
  List<Surah> allSurahs = [];
  List<Surah> filteredSurahs = [];
  TextEditingController searchController = TextEditingController();
  bool isSearching = false;
  String? lastReadSurah;
  String? lastReadVerse;

  Future<List<Surah>> loadSuraData() async {
    String jsonString =
        await rootBundle.loadString('assets/data/surah_db.json');
    List<dynamic> jsonResponse = json.decode(jsonString) as List;
    allSurahs = jsonResponse.map((item) => Surah.fromJson(item)).toList();
    filteredSurahs = List.from(allSurahs);
    return allSurahs;
  }

  void filterSurahs(String query) {
    setState(() {
      if (query.isEmpty) {
        filteredSurahs = List.from(allSurahs);
      } else {
        filteredSurahs = allSurahs.where((surah) {
          return surah.name.toLowerCase().contains(query.toLowerCase()) ||
              surah.transliteration
                  .toLowerCase()
                  .contains(query.toLowerCase()) ||
              surah.translation.toLowerCase().contains(query.toLowerCase()) ||
              surah.id.toString() == query;
        }).toList();
      }
    });
  }

  Future<void> _loadLastReadPosition() async {
    final prefs = await SharedPreferences.getInstance();
    String? surahName = prefs.getString('lastRead_activeSurah');
    
    if (surahName != null) {
      String? verse = prefs.getString('lastRead_$surahName');
      if (verse != null) {
        setState(() {
          lastReadSurah = surahName;
          lastReadVerse = verse.replaceAll('verse_', '');
        });
      }
    } else {
      setState(() {
        lastReadSurah = null;
        lastReadVerse = null;
      });
    }
  }

  void _navigateToLastRead(BuildContext context) async {
    if (lastReadSurah != null && lastReadVerse != null) {
      // البحث عن السورة المحددة
      final selectedSurah = allSurahs.firstWhere(
        (surah) => surah.name == lastReadSurah,
        orElse: () => allSurahs[0],
      );

      final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => QuranContentScreen(
            surahName: lastReadSurah!,
            initialVerse: lastReadVerse,
            surahNumber: selectedSurah.id,
          ),
        ),
      );

      // تحديث آخر قراءة عند العودة من شاشة المحتوى
      if (result != null && result is Map<String, dynamic>) {
        setState(() {
          lastReadSurah = result['surahName'];
          lastReadVerse = result['verseNumber'];
        });
        // تحديث واجهة المستخدم فوراً
        _loadLastReadPosition();
      }
    }
  }

  Future<void> _navigateToSurah(BuildContext context, Surah surah) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => QuranContentScreen(
          surahName: surah.name,
          surahNumber: surah.id,
        ),
      ),
    );

    // تحديث آخر قراءة عند العودة من شاشة المحتوى
    if (result != null && result is Map<String, dynamic>) {
      setState(() {
        lastReadSurah = result['surahName'];
        lastReadVerse = result['verseNumber'];
      });
      // تحديث واجهة المستخدم فوراً
      _loadLastReadPosition();
    }
  }

  @override
  void initState() {
    super.initState();
    surahsFuture = loadSuraData();
    _loadLastReadPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.grey[50],
        appBar: AppBar(
          elevation: 0,
          backgroundColor: AppColors.lightPrimary,
          title: !isSearching
              ? Text(
                  'القرآن الكريم',
                  style: TextStyle(
                    fontFamily: "Cairo",
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
              : TextField(
                  controller: searchController,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    hintText: 'ابحث عن سورة...',
                    hintStyle: TextStyle(
                      color: Colors.white70,
                      fontFamily: "Cairo",
                    ),
                    border: InputBorder.none,
                  ),
                  onChanged: filterSurahs,
                ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: Icon(
                isSearching ? Icons.close : Icons.search,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  if (isSearching) {
                    isSearching = false;
                    searchController.clear();
                    filterSurahs('');
                  } else {
                    isSearching = true;
                  }
                });
              },
            ),
          ],
        ),
        body: Column(
          children: [
            // إضافة بطاقة آخر قراءة في أعلى الشاشة
            if (lastReadSurah != null && lastReadVerse != null)
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: GestureDetector(
                  onTap: () => _navigateToLastRead(context),
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          AppColors.lightPrimary,
                          AppColors.lightPrimary.withOpacity(0.8),
                        ],
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                      ),
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: AppColors.lightPrimary.withOpacity(0.3),
                          blurRadius: 10,
                          offset: Offset(0, 5),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Icon(
                            Icons.bookmark,
                            color: Colors.white,
                            size: 32,
                          ),
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'آخر قراءة',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Cairo',
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(height: 4),
                              Text(
                                'سورة $lastReadSurah - الآية $lastReadVerse',
                                style: TextStyle(
                                  color: Colors.white.withOpacity(0.9),
                                  fontFamily: 'Cairo',
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 20,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            FutureBuilder<List<Surah>>(
              future: surahsFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(AppColors.lightPrimary),
                    ),
                  );
                }
                if (snapshot.hasError) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.error_outline, size: 48, color: Colors.red),
                        SizedBox(height: 16),
                        Text(
                          'حدث خطأ في تحميل السور',
                          style: TextStyle(
                            fontFamily: "Cairo",
                            fontSize: 18,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  );
                }

                return Expanded(
                  child: AnimationLimiter(
                    child: ListView.builder(
                      padding: EdgeInsets.all(16),
                      itemCount: filteredSurahs.length,
                      itemBuilder: (context, index) {
                        Surah surah = filteredSurahs[index];
                        return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: Duration(milliseconds: 375),
                          child: SlideAnimation(
                            verticalOffset: 50.0,
                            child: FadeInAnimation(
                              child: Card(
                                elevation: 2,
                                margin: EdgeInsets.only(bottom: 12),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(15),
                                  onTap: () {
                                    _navigateToSurah(context, surah);
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.all(16),
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 45,
                                          height: 45,
                                          decoration: BoxDecoration(
                                            color: AppColors.lightPrimary
                                                .withOpacity(0.1),
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: Center(
                                            child: Text(
                                              surah.id.toString(),
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                                color: AppColors.lightPrimary,
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 16),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                surah.name,
                                                style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: "Cairo",
                                                ),
                                              ),
                                              SizedBox(height: 4),
                                              Text(
                                                '${surah.transliteration}',
                                                style: TextStyle(
                                                  color: Colors.grey[600],
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Text(
                                              surah.type,
                                              style: TextStyle(
                                                color: AppColors.lightPrimary,
                                                fontFamily: "Cairo",
                                                fontSize: 14,
                                              ),
                                            ),
                                            SizedBox(height: 4),
                                            Text(
                                              '${surah.totalVerses} آيات',
                                              style: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: "Cairo",
                                                fontSize: 14,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
