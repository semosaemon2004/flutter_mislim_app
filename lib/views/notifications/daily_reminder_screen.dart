import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DailyRemindersScreen extends StatefulWidget {
  @override
  _DailyRemindersScreenState createState() => _DailyRemindersScreenState();
}

class _DailyRemindersScreenState extends State<DailyRemindersScreen> {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Map<String, TimeOfDay> selectedTimes = {
    'أذكار الصباح': TimeOfDay(hour: 6, minute: 0),
    'أذكار المساء': TimeOfDay(hour: 18, minute: 0),
    'أذكار الاستيقاظ': TimeOfDay(hour: 5, minute: 30),
    'أذكار النوم': TimeOfDay(hour: 22, minute: 0),
    'قيام الليل': TimeOfDay(hour: 3, minute: 0),
    'الورد القرآني': TimeOfDay(hour: 16, minute: 0),
    'سورة الملك': TimeOfDay(hour: 22, minute: 40),
  };

  Map<String, bool> isReminderActive = {};

  @override
  void initState() {
    super.initState();
    _initializeNotifications();
    _loadReminderStates();
  }

  Future<void> _loadReminderStates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      selectedTimes.keys.forEach((key) {
        isReminderActive[key] = prefs.getBool(key) ?? false;
        int? hour = prefs.getInt('${key}_hour');
        int? minute = prefs.getInt('${key}_minute');
        if (hour != null && minute != null) {
          selectedTimes[key] = TimeOfDay(hour: hour, minute: minute);
        }
        if (isReminderActive[key] == true) {
          _scheduleNotification(key, selectedTimes[key]!);
        }
      });
    });
  }

  Future<void> _saveReminderState(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
  }

  Future<void> _saveSelectedTime(String key, TimeOfDay time) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('${key}_hour', time.hour);
    await prefs.setInt('${key}_minute', time.minute);
  }

  Future<void> _initializeNotifications() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    final DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
      defaultPresentSound: true,
      defaultPresentAlert: true,
    );

    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse:
          (NotificationResponse notificationResponse) async {
        // يمكنك إضافة إجراء عند النقر على الإشعار
      },
    );

    if (Platform.isAndroid) {
      final AndroidFlutterLocalNotificationsPlugin? androidImplementation =
          flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>();

      await androidImplementation?.requestNotificationsPermission();

      AndroidNotificationChannel channel = AndroidNotificationChannel(
        'daily_reminders',
        'Daily Reminders',
        description: 'Channel for daily reminders',
        importance: Importance.max,
        enableVibration: true,
        enableLights: true,
        playSound: true,
        ledColor: const Color.fromARGB(255, 255, 0, 0),
        vibrationPattern: Int64List.fromList([0, 1000, 500, 1000]),
      );

      await androidImplementation?.createNotificationChannel(channel);
    }
  }

  Future<void> _scheduleNotification(
      String title, TimeOfDay selectedTime) async {
    if (!isReminderActive[title]!) return;

    await flutterLocalNotificationsPlugin
        .cancel(selectedTimes.keys.toList().indexOf(title));

    final now = DateTime.now();
    DateTime notificationTime = DateTime(
      now.year,
      now.month,
      now.day,
      selectedTime.hour,
      selectedTime.minute,
    );

    if (notificationTime.isBefore(now)) {
      notificationTime = notificationTime.add(Duration(days: 1));
    }

    final int delayInSeconds = notificationTime.difference(now).inSeconds;

    final AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'daily_reminders',
      'Daily Reminders',
      channelDescription: 'Channel for daily reminders',
      importance: Importance.max,
      priority: Priority.high,
      enableVibration: true,
      enableLights: true,
      playSound: true,
      ticker: 'تذكير',
      fullScreenIntent: true,
      category: AndroidNotificationCategory.alarm,
      visibility: NotificationVisibility.public,
    );

    final DarwinNotificationDetails iOSPlatformChannelSpecifics =
        DarwinNotificationDetails(
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
      badgeNumber: 1,
      interruptionLevel: InterruptionLevel.timeSensitive,
    );

    final NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );

    Future.delayed(Duration(seconds: delayInSeconds), () async {
      if (isReminderActive[title]!) {
        await flutterLocalNotificationsPlugin.show(
          selectedTimes.keys.toList().indexOf(title),
          'تذكير: $title',
          'حان الآن موعد $title',
          platformChannelSpecifics,
        );

        _scheduleNotification(title, selectedTime);
      }
    });
  }

  Future<void> _toggleReminder(String title, bool value) async {
    setState(() {
      isReminderActive[title] = value;
    });
    await _saveReminderState(title, value);

    if (value) {
      await _scheduleNotification(title, selectedTimes[title]!);
    } else {
      await flutterLocalNotificationsPlugin
          .cancel(selectedTimes.keys.toList().indexOf(title));
    }
  }

  void _showTimePicker(BuildContext context, String title) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          child: Container(
            padding: EdgeInsets.all(24),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'اختر وقت $title',
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Cairo",
                    color: AppColors.darkPrimary,
                  ),
                ),
                SizedBox(height: 24),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                  decoration: BoxDecoration(
                    color: AppColors.lightBackground,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(
                      color: AppColors.lightPrimary.withOpacity(0.3),
                      width: 2,
                    ),
                  ),
                  child: TimePickerSpinner(
                    is24HourMode: true,
                    normalTextStyle: TextStyle(
                      fontSize: 20,
                      color: Colors.grey[400],
                      fontFamily: "Cairo",
                    ),
                    highlightedTextStyle: TextStyle(
                      fontSize: 24,
                      color: AppColors.darkPrimary,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Cairo",
                    ),
                    spacing: 50,
                    itemHeight: 50,
                    isForce2Digits: true,
                    onTimeChange: (time) async {
                      TimeOfDay newTime = TimeOfDay(
                        hour: time.hour,
                        minute: time.minute,
                      );
                      setState(() {
                        selectedTimes[title] = newTime;
                      });
                      await _saveSelectedTime(title, newTime);

                      if (isReminderActive[title] == true) {
                        await _scheduleNotification(title, newTime);
                      }
                    },
                  ),
                ),
                SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text(
                        'إلغاء',
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Cairo",
                          color: Colors.grey[600],
                        ),
                      ),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.lightPrimary,
                        padding:
                            EdgeInsets.symmetric(horizontal: 36, vertical: 12),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'تم',
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Cairo",
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        appBar: AppBar(
          elevation: 0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [AppColors.darkPrimary, AppColors.lightPrimary],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
          title: Text(
            'الأذكار اليومية',
            style: TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              fontSize: 24,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(16),
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      AppColors.lightPrimary.withOpacity(0.9),
                      AppColors.darkPrimary.withOpacity(0.9),
                    ],
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                  ),
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.darkPrimary.withOpacity(0.2),
                      blurRadius: 10,
                      offset: Offset(0, 5),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Icon(
                      Icons.notifications_active,
                      color: Colors.white,
                      size: 32,
                    ),
                    SizedBox(height: 12),
                    Text(
                      'اختر الأوقات المناسبة للتذكير',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: "Cairo",
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  itemCount: selectedTimes.length,
                  itemBuilder: (context, index) {
                    String title = selectedTimes.keys.elementAt(index);
                    TimeOfDay time = selectedTimes[title]!;
                    return Container(
                      margin: EdgeInsets.only(bottom: 12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: [
                          BoxShadow(
                            color: AppColors.darkPrimary.withOpacity(0.1),
                            blurRadius: 8,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: ListTile(
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                        title: Text(
                          title,
                          style: TextStyle(
                            fontFamily: "Cairo",
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        subtitle: Row(
                          children: [
                            Icon(
                              Icons.access_time,
                              size: 16,
                              color: AppColors.lightPrimary,
                            ),
                            SizedBox(width: 4),
                            Text(
                              '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}',
                              style: TextStyle(
                                fontFamily: "Cairo",
                                color: AppColors.lightPrimary,
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                        trailing: Switch(
                          value: isReminderActive[title] ?? false,
                          onChanged: (bool value) {
                            _toggleReminder(title, value);
                          },
                          activeColor: AppColors.lightPrimary,
                          activeTrackColor:
                              AppColors.lightPrimary.withOpacity(0.3),
                        ),
                        onTap: () => _showTimePicker(context, title),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
