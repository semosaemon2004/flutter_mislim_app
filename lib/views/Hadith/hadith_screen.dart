import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:my_islamic_app/models/hadith.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:my_islamic_app/views/Hadith/hadith_content_screen.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class HadithScreen extends StatefulWidget {
  HadithScreen({super.key});

  @override
  State<HadithScreen> createState() => _HadithScreenState();
}

class _HadithScreenState extends State<HadithScreen> {
  late Future<List<Hadith>> hadithList;
  final TextEditingController _searchController = TextEditingController();
  String _searchQuery = "";

  Future<List<Hadith>> loadHadithData() async {
    String jsonString =
        await rootBundle.loadString('assets/data/hadith_40.json');
    List<dynamic> jsonResponse = json.decode(jsonString) as List;
    return jsonResponse.map((item) => Hadith.fromJson(item)).toList();
  }

  @override
  void initState() {
    super.initState();
    hadithList = loadHadithData();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        appBar: AppBar(
          title: const Text(
            'الأربعون النووية',
            style: TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: AppColors.lightPrimary,
          elevation: 0,
        ),
        body: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: AppColors.lightPrimary,
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'مجموعة من أهم الأحاديث النبوية',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Cairo",
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 16),
                  TextField(
                    controller: _searchController,
                    onChanged: (value) {
                      setState(() {
                        _searchQuery = value;
                      });
                    },
                    decoration: InputDecoration(
                      hintText: 'ابحث عن حديث...',
                      hintStyle: const TextStyle(
                        color: Colors.black54,
                        fontFamily: "Cairo",
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide.none,
                      ),
                      prefixIcon: const Icon(Icons.search),
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: FutureBuilder<List<Hadith>>(
                future: hadithList,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return Center(
                        child: Text(
                          'حدث خطأ: ${snapshot.error}',
                          style: const TextStyle(fontFamily: "Cairo"),
                        ),
                      );
                    }
                    if (snapshot.data == null || snapshot.data!.isEmpty) {
                      return const Center(
                        child: Text(
                          'لم يتم العثور على أحاديث',
                          style: TextStyle(fontFamily: "Cairo"),
                        ),
                      );
                    }

                    var filteredHadiths = snapshot.data!.where((hadith) {
                      return hadith.name
                          .toLowerCase()
                          .contains(_searchQuery.toLowerCase());
                    }).toList();

                    return AnimationLimiter(
                      child: GridView.builder(
                        padding: const EdgeInsets.all(16),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 0.85,
                          crossAxisSpacing: 16,
                          mainAxisSpacing: 16,
                        ),
                        itemCount: filteredHadiths.length,
                        itemBuilder: (context, index) {
                          Hadith hadith = filteredHadiths[index];
                          return AnimationConfiguration.staggeredGrid(
                            position: index,
                            duration: const Duration(milliseconds: 500),
                            columnCount: 2,
                            child: ScaleAnimation(
                              duration: const Duration(milliseconds: 900),
                              curve: Curves.fastLinearToSlowEaseIn,
                              child: FadeInAnimation(
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            HadithContentScreen(
                                          sectionId: hadith.id,
                                          title: hadith.name,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.1),
                                          blurRadius: 10,
                                          offset: const Offset(0, 5),
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.all(16),
                                          decoration: BoxDecoration(
                                            color: AppColors.lightPrimary
                                                .withOpacity(0.1),
                                            shape: BoxShape.circle,
                                          ),
                                          child: Image.asset(
                                            'assets/images/book.png',
                                            width: 50,
                                            height: 50,
                                          ),
                                        ),
                                        const SizedBox(height: 16),
                                        Text(
                                          "الحديث ${hadith.name}",
                                          style: const TextStyle(
                                            color: AppColors.darkPrimary,
                                            fontFamily: 'Cairo',
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        const SizedBox(height: 8),
                                        const Text(
                                          "انقر للقراءة",
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontFamily: 'Cairo',
                                            fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  }
                  return const Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(AppColors.lightPrimary),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
