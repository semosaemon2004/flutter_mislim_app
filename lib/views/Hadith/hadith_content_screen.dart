import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_islamic_app/models/hadith_content.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:share_plus/share_plus.dart';
import '../../widgets/font_size_menu.dart';

class HadithContentScreen extends StatefulWidget {
  final int sectionId;
  final String title;

  const HadithContentScreen({
    super.key,
    required this.sectionId,
    required this.title,
  });

  @override
  State<HadithContentScreen> createState() => _HadithContentScreenState();
}

class _HadithContentScreenState extends State<HadithContentScreen> {
  double _fontSize = 18.0;

  Future<HadithContent> loadHadithContent() async {
    final String response =
        await rootBundle.loadString('assets/data/hadith_40_content.json');
    final data = json.decode(response) as List;
    final hadithData = data.firstWhere(
      (hadith) => hadith['hadith_id'].toString() == widget.sectionId.toString(),
      orElse: () => null,
    );

    if (hadithData != null) {
      return HadithContent.fromJson(hadithData as Map<String, dynamic>);
    } else {
      return HadithContent(
        hadithId: widget.sectionId,
        content: 'عذراً، محتوى الحديث غير متوفر.',
      );
    }
  }

  void _shareHadith(BuildContext context, String content) {
    Share.share(
      'الحديث ${widget.title}\n\n$content\n\nمشاركة من تطبيق الإسلامي',
      subject: 'الحديث ${widget.title}',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        appBar: AppBar(
          title: Text(
            widget.title,
            style: const TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          backgroundColor: AppColors.lightPrimary,
          elevation: 0,
          actions: [
            IconButton(
              icon: const Icon(Icons.share),
              onPressed: () {
                // We need to get the hadith content first
                loadHadithContent().then((hadith) {
                  _shareHadith(context, hadith.content);
                });
              },
            ),
            FontSizeMenu(
              currentSize: _fontSize,
              onSelected: (double size) {
                setState(() => _fontSize = size);
              },
            ),
          ],
        ),
        body: FutureBuilder<HadithContent>(
          future: loadHadithContent(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                    'حدث خطأ: ${snapshot.error}',
                    style: const TextStyle(fontFamily: "Cairo"),
                  ),
                );
              }
              if (!snapshot.hasData) {
                return const Center(
                  child: Text(
                    'محتوى الحديث غير موجود',
                    style: TextStyle(fontFamily: "Cairo"),
                  ),
                );
              }

              final hadithContent = snapshot.data!;
              return SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(24),
                      decoration: BoxDecoration(
                        color: AppColors.lightPrimary,
                        borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30),
                        ),
                      ),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/book.png',
                            width: 60,
                            height: 60,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 16),
                          Text(
                            "الحديث ${widget.title}",
                            style: const TextStyle(
                              color: Colors.white,
                              fontFamily: "Cairo",
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                blurRadius: 10,
                                offset: const Offset(0, 5),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "نص الحديث:",
                                style: TextStyle(
                                  color: AppColors.darkPrimary,
                                  fontFamily: "Cairo",
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(height: 16),
                              Text(
                                hadithContent.content,
                                style: TextStyle(
                                  fontSize: _fontSize,
                                  height: 1.8,
                                  fontFamily: "Cairo",
                                  color: Colors.black87,
                                ),
                                textAlign: TextAlign.justify,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }
            return const Center(
              child: CircularProgressIndicator(
                valueColor:
                    AlwaysStoppedAnimation<Color>(AppColors.lightPrimary),
              ),
            );
          },
        ),
      ),
    );
  }
}
