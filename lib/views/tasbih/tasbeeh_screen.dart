import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_islamic_app/utils/colors.dart';

class TasbeehCounterScreen extends StatefulWidget {
  @override
  _TasbeehCounterScreenState createState() => _TasbeehCounterScreenState();
}

class _TasbeehCounterScreenState extends State<TasbeehCounterScreen> {
  List<String> tasbeehs = ['سبحان الله', 'الحمد لله', 'الله أكبر'];
  Map<String, int> tasbeehCounts = {};
  String selectedTasbeeh = 'سبحان الله';

  @override
  void initState() {
    super.initState();
    _loadTasbeehCounts();
  }

  Future<void> _loadTasbeehCounts() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      for (var tasbeeh in tasbeehs) {
        tasbeehCounts[tasbeeh] = prefs.getInt(tasbeeh) ?? 0;
      }
    });
  }

  void _incrementCounter() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      tasbeehCounts[selectedTasbeeh] =
          (tasbeehCounts[selectedTasbeeh] ?? 0) + 1;
    });
    await prefs.setInt(selectedTasbeeh, tasbeehCounts[selectedTasbeeh]!);
  }

  void _resetCounter() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      tasbeehCounts[selectedTasbeeh] = 0;
    });
    await prefs.setInt(selectedTasbeeh, 0);
  }

  void _addTasbeeh(String tasbeeh) {
    setState(() {
      if (!tasbeehs.contains(tasbeeh)) {
        tasbeehs.add(tasbeeh);
        tasbeehCounts[tasbeeh] = 0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        appBar: AppBar(
          elevation: 0,
          title: const Text(
            'المسبحة الإلكترونية',
            style: TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          centerTitle: true,
          backgroundColor: AppColors.lightPrimary,
        ),
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                AppColors.lightPrimary,
                AppColors.lightBackground,
              ],
            ),
          ),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                const SizedBox(height: 20),
                // Tasbeeh Selection
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        blurRadius: 10,
                        offset: const Offset(0, 5),
                      ),
                    ],
                  ),
                  child: DropdownButton<String>(
                    isExpanded: true,
                    value: selectedTasbeeh,
                    icon: const Icon(Icons.keyboard_arrow_down, color: AppColors.lightPrimary),
                    style: const TextStyle(
                      color: AppColors.darkPrimary,
                      fontSize: 20,
                      fontFamily: "Cairo",
                    ),
                    underline: Container(),
                    items: tasbeehs.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        selectedTasbeeh = newValue!;
                      });
                    },
                  ),
                ),
                const SizedBox(height: 30),
                // Counter Display
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 40),
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        blurRadius: 10,
                        offset: const Offset(0, 5),
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Text(
                        'عدد التسبيح',
                        style: TextStyle(
                          color: AppColors.darkPrimary.withOpacity(0.7),
                          fontSize: 18,
                          fontFamily: "Cairo",
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        '${tasbeehCounts[selectedTasbeeh] ?? 0}',
                        style: const TextStyle(
                          color: AppColors.darkPrimary,
                          fontSize: 48,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Cairo",
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 40),
                // Counter Button
                GestureDetector(
                  onTap: _incrementCounter,
                  child: Container(
                    width: screenWidth * 0.6,
                    height: screenWidth * 0.6,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.lightPrimary,
                      boxShadow: [
                        BoxShadow(
                          color: AppColors.lightPrimary.withOpacity(0.3),
                          blurRadius: 20,
                          spreadRadius: 5,
                          offset: const Offset(0, 10),
                        ),
                      ],
                    ),
                    child: const Center(
                      child: Text(
                        'اضغط للتسبيح',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Cairo",
                        ),
                      ),
                    ),
                  ),
                ),
                const Spacer(),
                // Bottom Controls
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      _buildControlButton(
                        icon: Icons.refresh,
                        label: 'تصفير',
                        onPressed: _resetCounter,
                      ),
                      _buildControlButton(
                        icon: Icons.add,
                        label: 'إضافة ذكر',
                        onPressed: () => _showAddTasbeehDialog(context),
                      ),
                    ],
                  ),
                ),
                // Total Counter
                Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        blurRadius: 10,
                        offset: const Offset(0, -5),
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        'المجموع الكلي: ',
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Cairo",
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        '${tasbeehCounts.isNotEmpty ? tasbeehCounts.values.reduce((a, b) => a + b) : 0}',
                        style: const TextStyle(
                          color: AppColors.darkPrimary,
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Cairo",
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildControlButton({
    required IconData icon,
    required String label,
    required VoidCallback onPressed,
  }) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onPressed,
        borderRadius: BorderRadius.circular(15),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 10,
                offset: const Offset(0, 5),
              ),
            ],
          ),
          child: Row(
            children: [
              Icon(icon, color: AppColors.lightPrimary),
              const SizedBox(width: 8),
              Text(
                label,
                style: const TextStyle(
                  color: AppColors.darkPrimary,
                  fontSize: 16,
                  fontFamily: "Cairo",
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showAddTasbeehDialog(BuildContext context) {
    TextEditingController tasbeehController = TextEditingController();
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          title: const Text(
            'إضافة ذكر جديد',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: AppColors.darkPrimary,
            ),
          ),
          content: TextField(
            controller: tasbeehController,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              hintText: "أدخل الذكر",
              hintStyle: const TextStyle(fontFamily: "Cairo"),
              filled: true,
              fillColor: Colors.grey[100],
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none,
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'إلغاء',
                style: TextStyle(
                  color: Colors.grey,
                  fontFamily: "Cairo",
                ),
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(
              child: const Text(
                'إضافة',
                style: TextStyle(
                  color: AppColors.lightPrimary,
                  fontWeight: FontWeight.bold,
                  fontFamily: "Cairo",
                ),
              ),
              onPressed: () {
                if (tasbeehController.text.isNotEmpty) {
                  _addTasbeeh(tasbeehController.text);
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        );
      },
    );
  }
}
