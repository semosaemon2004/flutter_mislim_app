import 'package:flutter/material.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:my_islamic_app/widgets/nav_bar.dart';
import 'dart:async'; // Necessary for using Timer

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // Starts a timer to navigate to the next page after 3 seconds
    Timer(Duration(seconds: 3), _navigateToHome);
  }

  // This method will be triggered after the timer finishes
  void _navigateToHome() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_) => NavBar()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightBackground,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/logo.png",
              height: 300,
            ),
            const SizedBox(height: 20),
            const Text(
              'سبيل المسلم',
              style: TextStyle(
                fontSize: 24,
                color: AppColors.lightPrimary,
                fontWeight: FontWeight.bold,
                fontFamily: "Cairo",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
