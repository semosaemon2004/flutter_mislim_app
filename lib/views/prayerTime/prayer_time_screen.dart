import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:adhan/adhan.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_islamic_app/utils/colors.dart';

class PrayerTimeScreen extends StatefulWidget {
  @override
  _PrayerTimeScreenState createState() => _PrayerTimeScreenState();
}

class _PrayerTimeScreenState extends State<PrayerTimeScreen> {
  PrayerTimes? prayerTimes;
  bool isLoading = true;
  String? errorMessage;

  @override
  void initState() {
    super.initState();
    _initializeLocale();
  }

  Future<void> _initializeLocale() async {
    await initializeDateFormatting('ar', null);
    _loadPrayerTimes();
  }

  Future<void> _loadPrayerTimes() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final String? prayerTimesString = prefs.getString('prayerTimes');
      final String? dateString = prefs.getString('prayerTimesDate');
      final DateTime now = DateTime.now();

      if (prayerTimesString != null && dateString != null) {
        final DateTime savedDate = DateTime.parse(dateString);
        if (savedDate.day == now.day &&
            savedDate.month == now.month &&
            savedDate.year == now.year) {
          final Map<String, dynamic> prayerTimesJson =
              json.decode(prayerTimesString);

          try {
            final coordinates = Coordinates(
              double.parse(prayerTimesJson['latitude'].toString()),
              double.parse(prayerTimesJson['longitude'].toString()),
            );

            final params =
                CalculationMethod.muslim_world_league.getParameters();
            params.madhab = Madhab.shafi;
            final date = DateComponents.from(now);

            setState(() {
              prayerTimes = PrayerTimes(coordinates, date, params);
              isLoading = false;
            });
            return;
          } catch (e) {
            print('Error parsing saved prayer times: $e');
          }
        }
      }
    } catch (e) {
      print('Error loading saved prayer times: $e');
    }

    _getPrayerTimes();
  }

  Future<void> _getPrayerTimes() async {
    setState(() {
      isLoading = true;
      errorMessage = null;
    });

    var location = Location();

    try {
      bool _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          setError("خدمة الموقع معطلة");
          return;
        }
      }

      PermissionStatus _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          setError("لم يتم منح إذن الموقع");
          return;
        }
      }

      final _locationData = await location.getLocation();

      if (_locationData.latitude == null || _locationData.longitude == null) {
        setError("تعذر تحديد الموقع");
        return;
      }

      final coordinates =
          Coordinates(_locationData.latitude!, _locationData.longitude!);
      final params = CalculationMethod.muslim_world_league.getParameters();
      params.madhab = Madhab.shafi;
      final date = DateComponents.from(DateTime.now());

      final newPrayerTimes = PrayerTimes(coordinates, date, params);

      setState(() {
        prayerTimes = newPrayerTimes;
        isLoading = false;
      });

      // Save the prayer times
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString(
          'prayerTimes',
          json.encode({
            'latitude': coordinates.latitude.toString(),
            'longitude': coordinates.longitude.toString(),
          }));
      await prefs.setString(
          'prayerTimesDate', DateTime.now().toIso8601String());
    } catch (e) {
      setError("حدث خطأ أثناء تحديث أوقات الصلاة");
      print('Error getting prayer times: $e');
    }
  }

  void setError(String message) {
    setState(() {
      isLoading = false;
      errorMessage = message;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightBackground,
      appBar: AppBar(
        elevation: 0,
        title: const Text(
          "مواقيت الصلاة",
          style: TextStyle(
            fontFamily: "Cairo",
            fontWeight: FontWeight.bold,
            color: Colors.white,
            fontSize: 24,
          ),
        ),
        centerTitle: true,
        backgroundColor: AppColors.lightPrimary,
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: _getPrayerTimes,
            tooltip: 'تحديث المواقيت',
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              AppColors.lightPrimary,
              AppColors.lightBackground,
            ],
          ),
        ),
        child: isLoading
            ? const Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(
                      color: Colors.white,
                    ),
                    SizedBox(height: 20),
                    Text(
                      'جاري تحميل مواقيت الصلاة...',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Cairo",
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              )
            : errorMessage != null
                ? Center(
                    child: Container(
                      margin: const EdgeInsets.all(20),
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 10,
                            offset: const Offset(0, 5),
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 50,
                          ),
                          const SizedBox(height: 20),
                          Text(
                            "حدث خطأ",
                            style: TextStyle(
                              color: Colors.red[700],
                              fontSize: 22,
                              fontFamily: "Cairo",
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Text(
                            errorMessage!,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Colors.black87,
                              fontSize: 16,
                              fontFamily: "Cairo",
                            ),
                          ),
                          const SizedBox(height: 20),
                          ElevatedButton(
                            onPressed: _getPrayerTimes,
                            style: ElevatedButton.styleFrom(
                              backgroundColor: AppColors.lightPrimary,
                              padding: const EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 12,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            child: const Text(
                              'إعادة المحاولة',
                              style: TextStyle(
                                fontFamily: "Cairo",
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : _buildPrayerTimesList(),
      ),
    );
  }

  Widget _buildPrayerTimesList() {
    if (prayerTimes == null) {
      return const Center(
        child: Text(
          "تعذر تحميل مواقيت الصلاة",
          style: TextStyle(
            color: Colors.white,
            fontFamily: "Cairo",
            fontSize: 18,
          ),
        ),
      );
    }

    final now = DateTime.now();
    final prayers = [
      {'name': 'fajr', 'time': prayerTimes!.fajr, 'displayName': 'الفجر'},
      {
        'name': 'sunrise',
        'time': prayerTimes!.sunrise,
        'displayName': 'الشروق'
      },
      {'name': 'dhuhr', 'time': prayerTimes!.dhuhr, 'displayName': 'الظهر'},
      {'name': 'asr', 'time': prayerTimes!.asr, 'displayName': 'العصر'},
      {
        'name': 'maghrib',
        'time': prayerTimes!.maghrib,
        'displayName': 'المغرب'
      },
      {'name': 'isha', 'time': prayerTimes!.isha, 'displayName': 'العشاء'},
    ];

    var nextPrayer = prayers.firstWhere(
      (prayer) => (prayer['time'] as DateTime).isAfter(now),
      orElse: () => prayers.first,
    );

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            // Current Date
            Container(
              margin: const EdgeInsets.only(bottom: 20),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: Text(
                DateFormat.yMMMMEEEEd('ar').format(now),
                style: const TextStyle(
                  fontSize: 18,
                  fontFamily: "Cairo",
                  fontWeight: FontWeight.bold,
                  color: AppColors.darkPrimary,
                ),
              ),
            ),

            // Next Prayer Time
            Container(
              margin: const EdgeInsets.only(bottom: 30),
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: AppColors.lightPrimary.withOpacity(0.9),
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: AppColors.lightPrimary.withOpacity(0.3),
                    blurRadius: 15,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: Column(
                children: [
                  const Text(
                    'الصلاة القادمة',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontFamily: "Cairo",
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    nextPrayer['displayName'] as String,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontFamily: "Cairo",
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 5),
                  Text(
                    DateFormat('hh:mm a')
                        .format(nextPrayer['time'] as DateTime),
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontFamily: "Cairo",
                    ),
                  ),
                ],
              ),
            ),

            // All Prayer Times
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: Column(
                children: prayers.map((prayer) {
                  return Column(
                    children: [
                      _buildPrayerTimeRow(
                        prayer['displayName'] as String,
                        prayer['time'] as DateTime,
                        prayer['name'] == nextPrayer['name'],
                      ),
                      if (prayers.last != prayer) _buildDivider(),
                    ],
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDivider() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: const Divider(height: 1, color: Colors.grey),
    );
  }

  Widget _buildPrayerTimeRow(String prayerName, DateTime time, bool isCurrent) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: BoxDecoration(
        color: isCurrent
            ? AppColors.lightPrimary.withOpacity(0.1)
            : Colors.transparent,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(
                Icons.access_time,
                color: isCurrent ? AppColors.lightPrimary : Colors.grey,
                size: 20,
              ),
              const SizedBox(width: 10),
              Text(
                DateFormat('hh:mm a').format(time),
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  color: isCurrent ? AppColors.lightPrimary : Colors.black87,
                  fontFamily: "Cairo",
                ),
              ),
            ],
          ),
          Text(
            prayerName,
            style: TextStyle(
              fontSize: 18,
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: isCurrent ? AppColors.lightPrimary : Colors.black87,
            ),
          ),
        ],
      ),
    );
  }
}
