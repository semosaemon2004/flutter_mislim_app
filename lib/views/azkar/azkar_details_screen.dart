import 'package:flutter/material.dart';
import 'package:my_islamic_app/models/sections_details_azkar.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:my_islamic_app/utils/colors.dart';
import 'dart:convert';
import 'package:share_plus/share_plus.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import '../../widgets/font_size_menu.dart';

class AzkarDetailsScreen extends StatefulWidget {
  final int sectionId;
  final String title;

  const AzkarDetailsScreen({Key? key, required this.sectionId, required this.title})
      : super(key: key);

  @override
  _AzkarDetailsScreenState createState() => _AzkarDetailsScreenState();
}

class _AzkarDetailsScreenState extends State<AzkarDetailsScreen> {
  double _fontSize = 18.0;

  Future<List<SectiosDetailsAzkar>> loadAzkarDetails() async {
    final String response =
        await rootBundle.loadString('assets/data/sections_details_azkar_db.json');
    final List<dynamic> data = json.decode(response) as List<dynamic>;
    return data.map((json) => SectiosDetailsAzkar.fromJson(json)).toList();
  }

  void _shareAzkar(BuildContext context, String content) {
    Share.share(
      '$content\n\nمشاركة من تطبيق الإسلامي',
      subject: widget.title,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        appBar: AppBar(
          title: Text(
            widget.title,
            style: const TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          backgroundColor: AppColors.lightPrimary,
          elevation: 0,
          actions: [
            FontSizeMenu(
              currentSize: _fontSize,
              onSelected: (double size) {
                setState(() => _fontSize = size);
              },
            ),
          ],
        ),
        body: Column(
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: AppColors.lightPrimary,
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              child: const Text(
                'اللَّهُمَّ اجْعَلْ فِي قَلْبِي نُورًا',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: "Cairo",
                  fontSize: 16,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: FutureBuilder<List<SectiosDetailsAzkar>>(
                future: loadAzkarDetails(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return Center(
                        child: Text(
                          'حدث خطأ: ${snapshot.error}',
                          style: const TextStyle(fontFamily: "Cairo"),
                        ),
                      );
                    }

                    final azkarDetails = snapshot.data
                        ?.where((azkar) => azkar.sectionId == widget.sectionId)
                        .toList();

                    if (azkarDetails == null || azkarDetails.isEmpty) {
                      return const Center(
                        child: Text(
                          'لم يتم العثور على أذكار في هذا القسم',
                          style: TextStyle(fontFamily: "Cairo"),
                        ),
                      );
                    }

                    return AnimationLimiter(
                      child: ListView.builder(
                        padding: const EdgeInsets.all(16),
                        itemCount: azkarDetails.length,
                        itemBuilder: (context, index) {
                          final azkar = azkarDetails[index];
                          return AnimationConfiguration.staggeredList(
                            position: index,
                            duration: const Duration(milliseconds: 500),
                            child: SlideAnimation(
                              verticalOffset: 50.0,
                              child: FadeInAnimation(
                                child: Container(
                                  margin: const EdgeInsets.only(bottom: 20),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.1),
                                        blurRadius: 10,
                                        offset: const Offset(0, 5),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(20),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              azkar.content ?? '',
                                              style: TextStyle(
                                                fontSize: _fontSize,
                                                height: 1.8,
                                                fontFamily: "Cairo",
                                                color: Colors.black87,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            if (azkar.description != null &&
                                                azkar.description!.isNotEmpty)
                                              Padding(
                                                padding: const EdgeInsets.only(top: 12),
                                                child: Text(
                                                  azkar.description!,
                                                  style: const TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.grey,
                                                    fontFamily: "Cairo",
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 20,
                                          vertical: 15,
                                        ),
                                        decoration: BoxDecoration(
                                          color: AppColors.lightPrimary.withOpacity(0.1),
                                          borderRadius: const BorderRadius.only(
                                            bottomLeft: Radius.circular(20),
                                            bottomRight: Radius.circular(20),
                                          ),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                const Icon(
                                                  Icons.repeat,
                                                  color: AppColors.lightPrimary,
                                                  size: 20,
                                                ),
                                                const SizedBox(width: 8),
                                                Text(
                                                  'التكرار: ${azkar.count ?? "1"}',
                                                  style: const TextStyle(
                                                    color: AppColors.darkPrimary,
                                                    fontFamily: "Cairo",
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            IconButton(
                                              icon: const Icon(
                                                Icons.share,
                                                color: AppColors.lightPrimary,
                                                size: 20,
                                              ),
                                              onPressed: () => _shareAzkar(
                                                context,
                                                azkar.content ?? '',
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  }
                  return const Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(AppColors.lightPrimary),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
