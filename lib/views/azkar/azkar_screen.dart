import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:my_islamic_app/models/sections_azkar.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:my_islamic_app/views/azkar/azkar_details_screen.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class AzkarScreen extends StatefulWidget {
  AzkarScreen({super.key});

  @override
  State<AzkarScreen> createState() => _AzkarScreenState();
}

class _AzkarScreenState extends State<AzkarScreen> {
  late Future<List<Azkar>> azkarList;
  final TextEditingController _searchController = TextEditingController();
  String _searchQuery = "";

  Future<List<Azkar>> loadAzkarData() async {
    String jsonString = await rootBundle.loadString('assets/data/sections_azkar_db.json');
    List<dynamic> jsonResponse = json.decode(jsonString) as List;
    return jsonResponse.map((item) => Azkar.fromJson(item)).toList();
  }

  @override
  void initState() {
    super.initState();
    azkarList = loadAzkarData();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        appBar: AppBar(
          title: const Text(
            'الأذكار اليومية',
            style: TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: AppColors.lightPrimary,
          elevation: 0,
        ),
        body: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: AppColors.lightPrimary,
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'ذِكْرُ اللَّهِ تَعَالَى حَيَاةُ الْقُلُوبِ',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Cairo",
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 16),
                  TextField(
                    controller: _searchController,
                    onChanged: (value) {
                      setState(() {
                        _searchQuery = value;
                      });
                    },
                    decoration: InputDecoration(
                      hintText: 'ابحث عن ذكر...',
                      hintStyle: const TextStyle(
                        color: Colors.black54,
                        fontFamily: "Cairo",
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide.none,
                      ),
                      prefixIcon: const Icon(Icons.search),
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: FutureBuilder<List<Azkar>>(
                future: azkarList,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return Center(
                        child: Text(
                          'حدث خطأ: ${snapshot.error}',
                          style: const TextStyle(fontFamily: "Cairo"),
                        ),
                      );
                    }

                    var filteredAzkar = snapshot.data!.where((azkar) {
                      return azkar.name
                          .toLowerCase()
                          .contains(_searchQuery.toLowerCase());
                    }).toList();

                    return AnimationLimiter(
                      child: ListView.builder(
                        padding: const EdgeInsets.all(16),
                        itemCount: filteredAzkar.length,
                        itemBuilder: (context, index) {
                          Azkar azkar = filteredAzkar[index];
                          return AnimationConfiguration.staggeredList(
                            position: index,
                            duration: const Duration(milliseconds: 500),
                            child: SlideAnimation(
                              verticalOffset: 50.0,
                              child: FadeInAnimation(
                                child: Container(
                                  margin: const EdgeInsets.only(bottom: 16),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.1),
                                        blurRadius: 10,
                                        offset: const Offset(0, 5),
                                      ),
                                    ],
                                  ),
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(15),
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => AzkarDetailsScreen(
                                              sectionId: azkar.id,
                                              title: azkar.name,
                                            ),
                                          ),
                                        );
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.all(16),
                                        child: Row(
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.all(12),
                                              decoration: BoxDecoration(
                                                color: AppColors.lightPrimary.withOpacity(0.1),
                                                shape: BoxShape.circle,
                                              ),
                                              child: Icon(
                                                Icons.auto_awesome,
                                                color: AppColors.lightPrimary,
                                                size: 24,
                                              ),
                                            ),
                                            const SizedBox(width: 16),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    azkar.name,
                                                    style: const TextStyle(
                                                      color: AppColors.darkPrimary,
                                                      fontFamily: 'Cairo',
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                  const SizedBox(height: 4),
                                                  const Text(
                                                    'انقر للقراءة',
                                                    style: TextStyle(
                                                      color: Colors.grey,
                                                      fontFamily: 'Cairo',
                                                      fontSize: 12,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Icon(
                                              Icons.arrow_forward_ios,
                                              color: AppColors.lightPrimary,
                                              size: 16,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  }
                  return const Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(AppColors.lightPrimary),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
