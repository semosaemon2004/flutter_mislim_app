import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_qiblah/flutter_qiblah.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QiblahScreen extends StatefulWidget {
  const QiblahScreen({Key? key}) : super(key: key);

  @override
  _QiblahScreenState createState() => _QiblahScreenState();
}

class _QiblahScreenState extends State<QiblahScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> animation;
  final Location location = Location();
  bool isLoading = true;
  double? qiblahDirection;
  String? errorMessage;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    animation = Tween(begin: 0.0, end: 0.0).animate(_animationController);
    _checkLocationService();
  }

  Future<void> _checkLocationService() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _loadQiblahDirection();
  }

  Future<void> _loadQiblahDirection() async {
    final prefs = await SharedPreferences.getInstance();
    final String? qiblahDirectionString = prefs.getString('qiblahDirection');
    final String? dateString = prefs.getString('qiblahDirectionDate');
    final DateTime now = DateTime.now();

    if (qiblahDirectionString != null && dateString != null) {
      final DateTime savedDate = DateTime.parse(dateString);
      if (savedDate.day == now.day && savedDate.month == now.month && savedDate.year == now.year) {
        setState(() {
          qiblahDirection = double.parse(qiblahDirectionString);
          isLoading = false;
        });
        return;
      }
    }

    _fetchQiblahDirection();
  }

  Future<void> _fetchQiblahDirection() async {
    try {
      final qiblahDirectionStream = await FlutterQiblah.qiblahStream.first;
      final qiblahDirection = qiblahDirectionStream.qiblah;

      final prefs = await SharedPreferences.getInstance();
      prefs.setString('qiblahDirection', qiblahDirection.toString());
      prefs.setString('qiblahDirectionDate', DateTime.now().toIso8601String());

      setState(() {
        this.qiblahDirection = qiblahDirection;
        isLoading = false;
        errorMessage = null;
      });
    } catch (e) {
      setError("Error fetching Qiblah direction: $e");
    }
  }

  void setError(String message) {
    setState(() {
      isLoading = false;
      errorMessage = message;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        appBar: AppBar(
          elevation: 0,
          title: const Text(
            'اتجاه القبلة',
            style: TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          centerTitle: true,
          backgroundColor: AppColors.lightPrimary,
        ),
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                AppColors.lightPrimary,
                AppColors.lightBackground,
              ],
            ),
          ),
          child: isLoading
              ? const Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(
                        color: Colors.white,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'جاري تحديد اتجاه القبلة...',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: "Cairo",
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                )
              : errorMessage != null
                  ? Center(
                      child: Container(
                        margin: const EdgeInsets.all(20),
                        padding: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 10,
                              offset: const Offset(0, 5),
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Icon(
                              Icons.error_outline,
                              color: Colors.red,
                              size: 50,
                            ),
                            const SizedBox(height: 20),
                            Text(
                              "حدث خطأ",
                              style: TextStyle(
                                color: Colors.red[700],
                                fontSize: 22,
                                fontFamily: "Cairo",
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(height: 10),
                            Text(
                              errorMessage!,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: Colors.black87,
                                fontSize: 16,
                                fontFamily: "Cairo",
                              ),
                            ),
                            const SizedBox(height: 20),
                            ElevatedButton(
                              onPressed: _fetchQiblahDirection,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: AppColors.lightPrimary,
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 30,
                                  vertical: 12,
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              child: const Text(
                                'إعادة المحاولة',
                                style: TextStyle(
                                  fontFamily: "Cairo",
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : _buildQiblahDirectionView(),
        ),
      ),
    );
  }

  Widget _buildQiblahDirectionView() {
    return StreamBuilder<QiblahDirection>(
      stream: FlutterQiblah.qiblahStream,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(color: Colors.white),
                SizedBox(height: 20),
                Text(
                  'جاري تحديث اتجاه القبلة...',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Cairo",
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          );
        }

        if (snapshot.hasError) {
          return Center(
            child: Container(
              margin: const EdgeInsets.all(20),
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Icon(
                    Icons.error_outline,
                    color: Colors.red,
                    size: 50,
                  ),
                  const SizedBox(height: 20),
                  Text(
                    "خطأ في تحديد الاتجاه",
                    style: TextStyle(
                      color: Colors.red[700],
                      fontSize: 22,
                      fontFamily: "Cairo",
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    snapshot.error.toString(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 16,
                      fontFamily: "Cairo",
                    ),
                  ),
                ],
              ),
            ),
          );
        }

        final qiblahDirection = snapshot.data;

        if (qiblahDirection == null) {
          return Center(
            child: Container(
              margin: const EdgeInsets.all(20),
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: const Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.location_off,
                    color: Colors.orange,
                    size: 50,
                  ),
                  SizedBox(height: 20),
                  Text(
                    "تعذر تحديد اتجاه القبلة",
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 22,
                      fontFamily: "Cairo",
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "يرجى التأكد من تفعيل خدمات الموقع والبوصلة",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: 16,
                      fontFamily: "Cairo",
                    ),
                  ),
                ],
              ),
            ),
          );
        }

        final newDirection = qiblahDirection.qiblah * (pi / 180) * -1;
        animation = Tween(begin: animation.value, end: newDirection)
            .animate(_animationController);
        _animationController
          ..reset()
          ..forward();

        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 30,
                  vertical: 15,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 10,
                      offset: const Offset(0, 5),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    const Text(
                      'اتجاه القبلة',
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 18,
                        fontFamily: "Cairo",
                      ),
                    ),
                    const SizedBox(height: 5),
                    Text(
                      "${qiblahDirection.qiblah.toStringAsFixed(0)}°",
                      style: const TextStyle(
                        color: AppColors.darkPrimary,
                        fontSize: 36,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Cairo",
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 40),
              Container(
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.lightPrimary.withOpacity(0.3),
                      blurRadius: 20,
                      spreadRadius: 5,
                    ),
                  ],
                ),
                child: AnimatedBuilder(
                  animation: animation,
                  builder: (context, child) {
                    return Transform.rotate(
                      angle: animation.value,
                      child: Image.asset(
                        'assets/images/qibla.png',
                        width: 200,
                        height: 200,
                      ),
                    );
                  },
                ),
              ),
              const SizedBox(height: 40),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 10,
                ),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.9),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: const Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.info_outline,
                      color: AppColors.lightPrimary,
                    ),
                    SizedBox(width: 10),
                    Text(
                      'قم بتوجيه هاتفك نحو السهم',
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 16,
                        fontFamily: "Cairo",
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
