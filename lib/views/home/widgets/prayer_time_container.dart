import 'dart:async';
import 'package:flutter/material.dart';
import 'package:adhan/adhan.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:hijri/hijri_calendar.dart';

class PrayerTimeContainer extends StatefulWidget {
  final PrayerTimes prayerTimes;

  const PrayerTimeContainer({super.key, required this.prayerTimes});

  @override
  _PrayerTimeContainerState createState() => _PrayerTimeContainerState();
}

class _PrayerTimeContainerState extends State<PrayerTimeContainer> {
  late Timer _timer;
  late DateTime _now;
  late final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  late final HijriCalendar _hijriDate;
  late final (String, DateTime) _nextPrayerInfo;
  Duration _timeUntilNextPrayer = Duration.zero;

  @override
  void initState() {
    super.initState();
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    _initializeNotifications();
    _now = DateTime.now();
    _hijriDate = HijriCalendar.fromDate(_now);
    _nextPrayerInfo = _getNextPrayerInfo(_now);
    _updateTimeUntilNextPrayer();
    
    _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      final newNow = DateTime.now();
      if (newNow.minute != _now.minute) {
        _now = newNow;
        _updateTimeUntilNextPrayer();
        if (mounted) setState(() {});
      }
      _checkForPrayerTime();
    });
  }

  void _updateTimeUntilNextPrayer() {
    _timeUntilNextPrayer = _nextPrayerInfo.$2.difference(_now);
  }

  Future<void> _initializeNotifications() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final InitializationSettings initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
    );
  }

  void _checkForPrayerTime() {
    final (String prayerName, DateTime nextPrayerTime) =
        _getNextPrayerInfo(_now);
    if (_now.isAtSameMomentAs(nextPrayerTime)) {
      _sendPrayerNotification(prayerName);
    }
  }

  void _sendPrayerNotification(String prayerName) {
    flutterLocalNotificationsPlugin.show(
      0,
      'حان الآن موعد $prayerName',
      'تفضل بالصلاة',
      NotificationDetails(
        android: AndroidNotificationDetails(
          'prayer_times_channel',
          'Prayer Times',
          channelDescription: 'Channel for prayer time notifications',
          importance: Importance.max,
          priority: Priority.high,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
        color: Color(0xff0463ca),
        image: DecorationImage(
          image: AssetImage('assets/images/background5.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 80.0),
            child: Column(
              children: [
                Text(
                  _nextPrayerInfo.$1,
                  style: const TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: Color(0xffcee3f4),
                  ),
                ),
                const SizedBox(height: 8),
                Text(
                  DateFormat('hh:mm a').format(_nextPrayerInfo.$2),
                  style: const TextStyle(fontSize: 18, color: Color(0xffcee3f4)),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'باقي على الصلاة',
                      style: TextStyle(
                        fontSize: 18,
                        color: Color(0xffcee3f4),
                        fontFamily: "Cairo",
                      ),
                    ),
                    const SizedBox(height: 4),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text(
                        _formatDuration(_timeUntilNextPrayer),
                        style: const TextStyle(fontSize: 18, color: Color(0xffcee3f4)),
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Text(
                        'تاريخ اليوم',
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xffcee3f4),
                          fontFamily: "Cairo"
                        ),
                      ),
                    ),
                    const SizedBox(height: 4),
                    Text(
                      ' ${_getArabicDayName(_hijriDate.getDayName())}, ${_hijriDate.hDay} ${_getArabicMonthName(_hijriDate.hMonth)}, ${_hijriDate.hYear}',
                      style: const TextStyle(
                        fontSize: 18,
                        color: Color(0xffcee3f4),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _getArabicMonthName(int monthNumber) {
    final List<String> arabicMonths = [
      'محرم',
      'صفر',
      'ربيع الأول',
      'ربيع الثاني',
      'جمادى الأولى',
      'جمادى الآخرة',
      'رجب',
      'شعبان',
      'رمضان',
      'شوال',
      'ذو القعدة',
      'ذو الحجة'
    ];
    return arabicMonths[monthNumber - 1];
  }

  String _getArabicDayName(String englishDayName) {
    switch (englishDayName) {
      case 'Saturday':
        return 'السبت';
      case 'Sunday':
        return 'الأحد';
      case 'Monday':
        return 'الاثنين';
      case 'Tuesday':
        return 'الثلاثاء';
      case 'Wednesday':
        return 'الأربعاء';
      case 'Thursday':
        return 'الخميس';
      case 'Friday':
        return 'الجمعة';
      default:
        return englishDayName;
    }
  }

  (String, DateTime) _getNextPrayerInfo(DateTime now) {
    if (now.isBefore(widget.prayerTimes.fajr))
      return ('الفجر', widget.prayerTimes.fajr);
    if (now.isBefore(widget.prayerTimes.sunrise))
      return ('الشروق', widget.prayerTimes.sunrise);
    if (now.isBefore(widget.prayerTimes.dhuhr))
      return ('الظهر', widget.prayerTimes.dhuhr);
    if (now.isBefore(widget.prayerTimes.asr))
      return ('العصر', widget.prayerTimes.asr);
    if (now.isBefore(widget.prayerTimes.maghrib))
      return ('المغرب', widget.prayerTimes.maghrib);
    if (now.isBefore(widget.prayerTimes.isha))
      return ('العشاء', widget.prayerTimes.isha);
    return ('الفجر', widget.prayerTimes.fajr.add(Duration(days: 1)));
  }

  String _formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }
}
