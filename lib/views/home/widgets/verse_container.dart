import 'package:flutter/material.dart';
import 'package:my_islamic_app/models/ayaOrHadith.dart';
import 'package:my_islamic_app/utils/copy_share_utils.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class VerseContainer extends StatefulWidget {
  final List<AyaOrHadith> ayaOrHadiths;
  final int current;

  VerseContainer({required this.ayaOrHadiths, this.current = 0});

  @override
  _VerseContainerState createState() => _VerseContainerState();
}

class _VerseContainerState extends State<VerseContainer>
    with SingleTickerProviderStateMixin {
  late PageController _pageController;
  late AnimationController _animationController;
  late Animation<double> _fadeAnimation;
  int _current = 0;

  @override
  void initState() {
    super.initState();
    _current = widget.current;
    _pageController = PageController(initialPage: _current);
    _animationController = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    _fadeAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.easeIn),
    );
    _animationController.forward();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.ayaOrHadiths.isEmpty) {
      return Container(
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.white, Colors.grey],
          ),
          //borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 10,
              offset: Offset(0, 5),
            ),
          ],
        ),
        child: Column(
          children: [
            Text(
              'آية وحديث',
              style: TextStyle(
                fontSize: 26,
                fontWeight: FontWeight.bold,
                fontFamily: "Amiri",
                color: Color(0xff0463ca),
              ),
            ),
            SizedBox(height: 15),
            Text(
              'لا توجد آيات أو أحاديث متاحة.',
              style: TextStyle(
                fontSize: 20,
                color: Colors.white70,
                fontFamily: "Cairo",
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            blurRadius: 10,
            offset: Offset(0, 5),
          ),
        ],
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        copyToClipboard(
                            context, widget.ayaOrHadiths[_current].text);
                      },
                      icon: Icon(
                        Icons.copy_rounded,
                        color: Color(0xff0463ca),
                      ),
                      tooltip: 'نسخ',
                    ),
                    IconButton(
                      onPressed: () {
                        shareText(context, widget.ayaOrHadiths[_current].text);
                      },
                      icon: Icon(
                        Icons.share_rounded,
                        color: Color(0xff0463ca),
                      ),
                      tooltip: 'مشاركة',
                    ),
                  ],
                ),
                Text(
                  'آية وحديث',
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Amiri",
                    color: Color(0xff0463ca),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 250,
            child: PageView.builder(
              controller: _pageController,
              itemCount: widget.ayaOrHadiths.length,
              onPageChanged: (index) {
                setState(() {
                  _current = index;
                  _animationController.reset();
                  _animationController.forward();
                });
              },
              itemBuilder: (context, index) {
                return FadeTransition(
                  opacity: _fadeAnimation,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Color(0xff0463ca),
                        width: 1,
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Center(
                            child: Text(
                              widget.ayaOrHadiths[index].text,
                              style: TextStyle(
                                fontSize: 22,
                                height: 1.8,
                                fontFamily: "Amiri",
                                color: Color(0xff0463ca),
                              ),
                              textAlign: TextAlign.center,
                              textDirection: TextDirection.rtl,
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Text(
                          widget.ayaOrHadiths[index].explanation ?? '',
                          style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Cairo",
                            color: Color(0xff0463ca),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: SmoothPageIndicator(
              controller: _pageController,
              count: widget.ayaOrHadiths.length,
              effect: CustomizableEffect(
                activeDotDecoration: DotDecoration(
                  width: 32,
                  height: 8,
                  color: Color(0xff0463ca),
                  rotationAngle: 180,
                  borderRadius: BorderRadius.circular(24),
                ),
                dotDecoration: DotDecoration(
                  width: 8,
                  height: 8,
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(16),
                ),
                spacing: 6.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
