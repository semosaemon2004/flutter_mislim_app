import 'package:flutter/material.dart';
import 'package:my_islamic_app/models/messagesOfDay.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_islamic_app/utils/copy_share_utils.dart';

class MessageContainer extends StatefulWidget {
  @override
  _MessageContainerState createState() => _MessageContainerState();
}

class _MessageContainerState extends State<MessageContainer> {
  String dailyMessage = '';

  @override
  void initState() {
    super.initState();
    _loadDailyMessage();
  }

  Future<void> _loadDailyMessage() async {
    final prefs = await SharedPreferences.getInstance();
    final int lastIndex = prefs.getInt('lastMessageIndex') ?? 0;
    final String? lastUpdateDate = prefs.getString('lastMessageUpdateDate');
    final DateTime now = DateTime.now();

    if (lastUpdateDate != null &&
        DateTime.parse(lastUpdateDate).day == now.day &&
        DateTime.parse(lastUpdateDate).month == now.month &&
        DateTime.parse(lastUpdateDate).year == now.year) {
      setState(() {
        dailyMessage = prefs.getString('dailyMessage') ?? '';
      });
    } else {
      int newIndex = lastIndex + 1;
      if (newIndex >= messagesOfDayList.length) {
        newIndex = 0;
      }
      final newMessage = messagesOfDayList[newIndex].message;

      prefs.setInt('lastMessageIndex', newIndex);
      prefs.setString('dailyMessage', newMessage);
      prefs.setString('lastMessageUpdateDate', now.toIso8601String());

      setState(() {
        dailyMessage = newMessage;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final containerHeight = mediaQuery.size.height * 0.25;
    final containerWidth = mediaQuery.size.width * 0.3;

    return Stack(
      children: [
        Container(
          height: containerHeight,
          width: containerWidth,
          decoration: BoxDecoration(
            color: Color(0xff0463ca),
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(50),
              bottomRight: Radius.circular(50),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(right: containerWidth * 0.3),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: IconButton(
                    onPressed: () => copyToClipboard(context, dailyMessage),
                    icon: Icon(Icons.copy),
                    color: Color(0xff0463ca),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Padding(
                padding: EdgeInsets.only(right: containerWidth * 0.3),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: IconButton(
                    onPressed: () => shareText(context, dailyMessage),
                    icon: Icon(Icons.share),
                    color: Color(0xff0463ca),
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: containerWidth * 0.75,
          top: containerHeight * 0.1,
          bottom: containerHeight * 0.1,
          child: Container(
            width: mediaQuery.size.width - (containerWidth * 0.90),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                bottomLeft: Radius.circular(30),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  spreadRadius: 2,
                  blurRadius: 5,
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'رسالتك اليوم',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Cairo",
                    color: Color(0xff0463ca),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  dailyMessage,
                  style: TextStyle(
                    fontSize: 18,
                    fontStyle: FontStyle.italic,
                    color: const Color.fromARGB(255, 141, 0, 166),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
