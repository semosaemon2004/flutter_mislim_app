import 'package:flutter/material.dart';
import 'package:my_islamic_app/views/alsirahNabawia/alsirah_alnabawia_screen.dart';
import 'package:my_islamic_app/views/azkar/azkar_screen.dart';
import 'package:my_islamic_app/views/Hadith/hadith_screen.dart';
import 'package:my_islamic_app/views/qiblah/qiblah_screen.dart';
import 'package:my_islamic_app/views/quran/quran_content_screen.dart';
import 'package:my_islamic_app/views/quran/quran_screen.dart';
import 'package:my_islamic_app/views/tasbih/tasbeeh_screen.dart';

class GridViewContainer extends StatelessWidget {
  final List<Map<String, dynamic>> items = [
    {
      'icon': "assets/images/hadith.png",
      'label': 'الحديث',
      'onTap': (context) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HadithScreen()),
        );
      }
    },
    {
      'icon': "assets/images/dua.png",
      'label': 'الأذكار',
      'onTap': (context) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AzkarScreen()),
        );
      },
    },
    {
      'icon': "assets/images/quran.png",
      'label': 'القرآن الكريم',
      'onTap': (context) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => QuranScreen(),
          ),
        );
      }
    },
    {
      'icon': "assets/images/kaaba.png",
      'label': 'القبلة',
      'onTap': (context) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => QiblahScreen()),
        );
      }
    },
    {
      'icon': "assets/images/tasbih.png",
      'label': 'التسبيح',
      'onTap': (context) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TasbeehCounterScreen()),
        );
      }
    },
    {
      'icon': "assets/images/muhammad.png",
      'label': 'السيرة النبوية',
      'onTap': (context) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SirahNabawiaScreen()),
        );
      }
    },
  ];

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            spreadRadius: 1,
            blurRadius: 4,
          ),
        ],
      ),
      child: GridView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: mediaQuery.size.width > 600 ? 4 : 3,
          crossAxisSpacing: 15,
          mainAxisSpacing: 15,
        ),
        itemCount: items.length,
        itemBuilder: (context, index) {
          return _buildGridItem(
            items[index]['icon'],
            items[index]['label'],
            items[index]['onTap'],
            context,
          );
        },
      ),
    );
  }

  Widget _buildGridItem(
      String icon, String label, Function onTap, BuildContext context) {
    return InkWell(
      onTap: () => onTap(context),
      child: Column(
        children: [
          Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xffb0d6f5),
            ),
            child: Center(
              child: Image.asset(
                icon,
                width: 30,
                height: 30,
              ),
            ),
          ),
          SizedBox(height: 8),
          Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              fontFamily: "Cairo",
              color: Color(0xff0463ca),
            ),
          ),
        ],
      ),
    );
  }
}
