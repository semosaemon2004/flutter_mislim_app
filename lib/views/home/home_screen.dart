import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:adhan/adhan.dart';
import 'package:location/location.dart';
import 'package:my_islamic_app/views/notifications/daily_reminder_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_islamic_app/views/home/widgets/grid_view_container.dart';
import 'package:my_islamic_app/views/home/widgets/message_container.dart';
import 'package:my_islamic_app/views/home/widgets/prayer_time_container.dart';
import 'package:my_islamic_app/views/home/widgets/verse_container.dart';
import 'package:my_islamic_app/models/ayaOrHadith.dart';
import 'package:flutter_slider_drawer/flutter_slider_drawer.dart';
import '../../widgets/app_drawer.dart';

import '../../models/serializablePrayerTimes.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final ScrollController _scrollController = ScrollController();
  final GlobalKey<SliderDrawerState> _drawerKey =
      GlobalKey<SliderDrawerState>();
  final ValueNotifier<double> _blurSigmaNotifier = ValueNotifier<double>(0.0);

  PrayerTimes? prayerTimes;
  bool isLoading = true;
  String? errorMessage;
  List<AyaOrHadith> dailyAyaOrHadiths = [];
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _loadPrayerTimes();
    _loadDailyAyaOrHadiths();
  }

  void _onScroll() {
    _blurSigmaNotifier.value = _scrollController.offset > 0 ? 10.0 : 0.0;
  }

  Future<void> _loadDailyAyaOrHadiths() async {
    final prefs = await SharedPreferences.getInstance();
    final int lastIndex = prefs.getInt('lastIndex') ?? 0;
    final String? lastUpdateDate = prefs.getString('lastUpdateDate');
    final DateTime now = DateTime.now();

    if (lastUpdateDate != null &&
        DateTime.parse(lastUpdateDate).day == now.day) {
      setState(() {
        dailyAyaOrHadiths = ayaOrHadithList.sublist(
            lastIndex, (lastIndex + 3).clamp(0, ayaOrHadithList.length));
      });
    } else {
      int newIndex = lastIndex + 3;
      if (newIndex >= ayaOrHadithList.length) {
        newIndex = 0;
      }

      prefs.setInt('lastIndex', newIndex);
      prefs.setString('lastUpdateDate', now.toIso8601String());

      setState(() {
        dailyAyaOrHadiths = ayaOrHadithList.sublist(
            newIndex, (newIndex + 3).clamp(0, ayaOrHadithList.length));
      });
    }
  }

  Future<void> _loadPrayerTimes() async {
    final prefs = await SharedPreferences.getInstance();
    final String? prayerTimesString = prefs.getString('prayerTimes');
    final String? dateString = prefs.getString('prayerTimesDate');
    final DateTime now = DateTime.now();

    if (prayerTimesString != null && dateString != null) {
      final DateTime savedDate = DateTime.parse(dateString);
      if (savedDate.day == now.day &&
          savedDate.month == now.month &&
          savedDate.year == now.year) {
        final Map<String, dynamic> prayerTimesJson =
            json.decode(prayerTimesString);
        setState(() {
          prayerTimes =
              SerializablePrayerTimes.fromJson(prayerTimesJson).toPrayerTimes();
          isLoading = false;
        });
        return;
      }
    }

    _getPrayerTimes();
  }

  Future<void> _getPrayerTimes() async {
    var location = Location();

    try {
      bool _serviceEnabled;
      PermissionStatus _permissionGranted;
      LocationData _locationData;

      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          setError("Location service is disabled.");
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          setError("Location permission not granted.");
          return;
        }
      }

      _locationData = await location.getLocation();
      final myLocation =
          Coordinates(_locationData.latitude!, _locationData.longitude!);
      final params = CalculationMethod.muslim_world_league.getParameters();
      params.madhab = Madhab.shafi;
      final date = DateComponents.from(DateTime.now());

      prayerTimes = PrayerTimes(myLocation, date, params);

      final prefs = await SharedPreferences.getInstance();
      final serializablePrayerTimes =
          SerializablePrayerTimes.fromPrayerTimes(prayerTimes!);
      prefs.setString(
          'prayerTimes', json.encode(serializablePrayerTimes.toJson()));
      prefs.setString('prayerTimesDate', DateTime.now().toIso8601String());

      setState(() {
        isLoading = false;
        errorMessage = null;
      });
    } catch (e) {
      setError("Error fetching location: $e");
    }
  }

  void setError(String message) {
    setState(() {
      isLoading = false;
      errorMessage = message;
    });
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: const Color(0xffdce6ee),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          SingleChildScrollView(
            controller: _scrollController,
            physics: const BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (isLoading)
                  const Center(child: CircularProgressIndicator())
                else if (errorMessage != null)
                  Center(child: Text(errorMessage!))
                else
                  PrayerTimeContainer(prayerTimes: prayerTimes!),
                SizedBox(height: mediaQuery.size.height * 0.02),
                GridViewContainer(),
                SizedBox(height: mediaQuery.size.height * 0.02),
                VerseContainer(ayaOrHadiths: dailyAyaOrHadiths),
                SizedBox(height: mediaQuery.size.height * 0.02),
                MessageContainer(),
                SizedBox(height: mediaQuery.size.height * 0.02),
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: ValueListenableBuilder<double>(
              valueListenable: _blurSigmaNotifier,
              builder: (context, blurSigma, child) => ClipRect(
                child: BackdropFilter(
                  filter:
                      ImageFilter.blur(sigmaX: blurSigma, sigmaY: blurSigma),
                  child: AppBar(
                    leadingWidth: 48,
                    leading: IconButton(
                      icon: const Icon(Icons.notification_add,
                          color: Colors.white),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DailyRemindersScreen(),
                          ),
                        );
                      },
                    ),
                    actions: [
                      IconButton(
                        icon: const Icon(Icons.menu, color: Colors.white),
                        onPressed: () => _showDrawer(context),
                      ),
                    ],
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showDrawer(BuildContext context) {
    showDialog(
      context: context,
      barrierColor: Colors.black54,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: EdgeInsets.zero,
          child: SizedBox(
            width: 280,
            child: AppDrawer(
              currentIndex: _currentIndex,
              onPageChanged: (index) {
                setState(() => _currentIndex = index);
                Navigator.pop(context);
              },
              drawerKey: _drawerKey,
            ),
          ),
        );
      },
    );
  }
}
