import 'package:flutter/material.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            "صدقة جارية",
            style: TextStyle(
              fontFamily: "Cairo",
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: AppColors.lightPrimary,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "شارك التطبيق ليصبح صدقة جارية خاصة بك بحيث يتناقله الاف الناس من بعدك .",
                style: TextStyle(
                  fontSize: 20,
                  fontFamily: "Cairo",
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 30),
              ElevatedButton(
                onPressed: () => _sendEmail(context),
                child: Text('Send Email'),
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () => _sendWhatsAppMessage(context),
                child: Text('Send WhatsApp Message'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _sendEmail(BuildContext context) async {
    final Uri emailLaunchUri =
        Uri.parse('mailto:semosaemon2004@gmail.com?subject=Example%20Subject');

    if (await canLaunchUrl(emailLaunchUri)) {
      await launchUrl(emailLaunchUri);
    } else {
      _showDialog(context, 'Unable to open email app');
    }
  }

  void _sendWhatsAppMessage(BuildContext context) async {
    final Uri whatsappUri = Uri(
      scheme: 'https',
      host: 'wa.me',
      path: '+963930444946', // Replace with your WhatsApp number
      queryParameters: {
        'text': 'Hello' // Pre-filled message (optional)
      },
    );

    if (await canLaunchUrl(whatsappUri)) {
      await launchUrl(whatsappUri);
    } else {
      // Handle the situation when WhatsApp can't be opened
      _showDialog(context, 'Unable to open WhatsApp');
    }
  }

  void _showDialog(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content: Text(message),
          actions: [
            TextButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
