import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:my_islamic_app/models/alsirah_alnabawia.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:my_islamic_app/views/alsirahNabawia/alsirah_alnabawia_content_screen.dart';

class SirahNabawiaScreen extends StatefulWidget {
  const SirahNabawiaScreen({super.key});

  @override
  State<SirahNabawiaScreen> createState() => _SirahNabawiaScreenState();
}

class _SirahNabawiaScreenState extends State<SirahNabawiaScreen> {
  late Future<List<SirahNabawia>> sirahNabawiaList;
  final TextEditingController _searchController = TextEditingController();
  String _searchQuery = "";

  Future<List<SirahNabawia>> loadSirahNabawiaData() async {
    String jsonString =
        await rootBundle.loadString('assets/data/alsirah_alnabawia.json');
    List<dynamic> jsonResponse = json.decode(jsonString) as List;
    return jsonResponse.map((item) => SirahNabawia.fromJson(item)).toList();
  }

  @override
  void initState() {
    super.initState();
    sirahNabawiaList = loadSirahNabawiaData();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              backgroundColor: AppColors.lightPrimary,
              flexibleSpace: FlexibleSpaceBar(
                title: const Text(
                  'السيرة النبوية',
                  style: TextStyle(
                    fontFamily: "Cairo",
                    fontWeight: FontWeight.bold,
                  ),
                ),
                background: Stack(
                  fit: StackFit.expand,
                  children: [
                    // Image.asset(
                    //   'assets/images/islamic_pattern.png',
                    //   fit: BoxFit.cover,
                    // ),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.transparent,
                            AppColors.lightPrimary.withOpacity(0.7),
                          ],
                        ),
                      ),
                    ),
                    const Positioned(
                      bottom: 60,
                      left: 0,
                      right: 0,
                      child: Text(
                        'وَإِنَّكَ لَعَلَىٰ خُلُقٍ عَظِيمٍ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontFamily: "Uthmanic",
                          shadows: [
                            Shadow(
                              offset: Offset(1.0, 1.0),
                              blurRadius: 3.0,
                              color: Color.fromARGB(255, 0, 0, 0),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextField(
                  controller: _searchController,
                  onChanged: (value) {
                    setState(() {
                      _searchQuery = value;
                    });
                  },
                  decoration: InputDecoration(
                    hintText: 'ابحث في السيرة النبوية...',
                    hintStyle: const TextStyle(
                      color: Colors.black54,
                      fontFamily: "Cairo",
                    ),
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                    prefixIcon: const Icon(Icons.search),
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 12,
                    ),
                  ),
                ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: FutureBuilder<List<SirahNabawia>>(
                future: sirahNabawiaList,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return SliverFillRemaining(
                        child: Center(
                          child: Text(
                            'حدث خطأ: ${snapshot.error}',
                            style: const TextStyle(fontFamily: "Cairo"),
                          ),
                        ),
                      );
                    }

                    var filteredSirah = snapshot.data!.where((sirah) {
                      return sirah.name
                          .toLowerCase()
                          .contains(_searchQuery.toLowerCase());
                    }).toList();

                    return AnimationLimiter(
                      child: SliverGrid(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount:
                              MediaQuery.of(context).size.width > 600 ? 3 : 2,
                          childAspectRatio: 1,
                          crossAxisSpacing: 16,
                          mainAxisSpacing: 16,
                        ),
                        delegate: SliverChildBuilderDelegate(
                          (context, index) {
                            final sirah = filteredSirah[index];
                            return AnimationConfiguration.staggeredGrid(
                              position: index,
                              duration: const Duration(milliseconds: 500),
                              columnCount:
                                  MediaQuery.of(context).size.width > 600
                                      ? 3
                                      : 2,
                              child: ScaleAnimation(
                                child: FadeInAnimation(
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                SirahNabawiaContentScreen(
                                              sectionId: sirah.id,
                                              title: sirah.name,
                                            ),
                                          ),
                                        );
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.black.withOpacity(0.1),
                                              blurRadius: 10,
                                              offset: const Offset(0, 5),
                                            ),
                                          ],
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.all(16),
                                              decoration: BoxDecoration(
                                                color: AppColors.lightPrimary
                                                    .withOpacity(0.1),
                                                shape: BoxShape.circle,
                                              ),
                                              child: Icon(
                                                Icons.auto_stories,
                                                size: 32,
                                                color: AppColors.lightPrimary,
                                              ),
                                            ),
                                            const SizedBox(height: 12),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 8),
                                              child: Text(
                                                sirah.name,
                                                style: const TextStyle(
                                                  color: AppColors.darkPrimary,
                                                  fontFamily: 'Cairo',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14,
                                                ),
                                                textAlign: TextAlign.center,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          childCount: filteredSirah.length,
                        ),
                      ),
                    );
                  }
                  return const SliverFillRemaining(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                            AppColors.lightPrimary),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
