import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_islamic_app/models/alsirah_alnabawia_content.dart';
import 'package:my_islamic_app/utils/colors.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:share_plus/share_plus.dart';

class SirahNabawiaContentScreen extends StatelessWidget {
  final int sectionId;
  final String title;

  const SirahNabawiaContentScreen({
    Key? key,
    required this.sectionId,
    required this.title,
  }) : super(key: key);

  Future<SirahNabawiaContent> loadContent() async {
    final String response = await rootBundle
        .loadString('assets/data/alsirah_alnabawia_content.json');
    final List<dynamic> data = json.decode(response) as List<dynamic>;
    final content = data.firstWhere(
      (item) => item['id'] == sectionId,
      orElse: () => throw Exception('محتوى غير موجود'),
    );
    return SirahNabawiaContent.fromJson(content);
  }

  void _shareContent(BuildContext context, SirahNabawiaContent content) {
    Share.share(
      '${content.title}\n\n${content.content}\n\nمشاركة من تطبيق الإسلامي',
      subject: content.title,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: AppColors.lightBackground,
        body: FutureBuilder<SirahNabawiaContent>(
          future: loadContent(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                    'حدث خطأ: ${snapshot.error}',
                    style: const TextStyle(fontFamily: "Cairo"),
                  ),
                );
              }

              final content = snapshot.data!;
              return CustomScrollView(
                slivers: [
                  SliverAppBar(
                    expandedHeight: 200.0,
                    floating: false,
                    pinned: true,
                    backgroundColor: AppColors.lightPrimary,
                    flexibleSpace: FlexibleSpaceBar(
                      title: Text(
                        title,
                        style: const TextStyle(
                          fontFamily: "Cairo",
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      background: Stack(
                        fit: StackFit.expand,
                        children: [
                          // Image.asset(
                          //   'assets/images/islamic_pattern.png',
                          //   fit: BoxFit.cover,
                          // ),
                          Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Colors.transparent,
                                  AppColors.lightPrimary.withOpacity(0.7),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    actions: [
                      IconButton(
                        icon: const Icon(
                          Icons.share,
                          color: Colors.white,
                        ),
                        onPressed: () => _shareContent(context, content),
                      ),
                    ],
                  ),
                  SliverToBoxAdapter(
                    child: AnimationLimiter(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: AnimationConfiguration.toStaggeredList(
                            duration: const Duration(milliseconds: 375),
                            childAnimationBuilder: (widget) => SlideAnimation(
                              horizontalOffset: 50.0,
                              child: FadeInAnimation(child: widget),
                            ),
                            children: [
                              if (content.date != null) ...[
                                _buildInfoCard(
                                  icon: Icons.calendar_today,
                                  title: 'التاريخ',
                                  content: content.date!,
                                ),
                                const SizedBox(height: 16),
                              ],
                              if (content.location != null) ...[
                                _buildInfoCard(
                                  icon: Icons.location_on,
                                  title: 'المكان',
                                  content: content.location!,
                                ),
                                const SizedBox(height: 16),
                              ],
                              Container(
                                padding: const EdgeInsets.all(16),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(12),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.1),
                                      blurRadius: 10,
                                      offset: const Offset(0, 5),
                                    ),
                                  ],
                                ),
                                child: Text(
                                  content.content,
                                  style: const TextStyle(
                                    fontSize: 18,
                                    height: 1.8,
                                    fontFamily: "Cairo",
                                    color: Colors.black87,
                                  ),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              if (content.keyEvents != null &&
                                  content.keyEvents!.isNotEmpty) ...[
                                const SizedBox(height: 24),
                                const Text(
                                  'أهم الأحداث',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "Cairo",
                                    color: AppColors.darkPrimary,
                                  ),
                                ),
                                const SizedBox(height: 16),
                                ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: content.keyEvents!.length,
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 8.0),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin:
                                                const EdgeInsets.only(top: 8),
                                            height: 8,
                                            width: 8,
                                            decoration: const BoxDecoration(
                                              color: AppColors.lightPrimary,
                                              shape: BoxShape.circle,
                                            ),
                                          ),
                                          const SizedBox(width: 16),
                                          Expanded(
                                            child: Text(
                                              content.keyEvents![index],
                                              style: const TextStyle(
                                                fontSize: 16,
                                                height: 1.6,
                                                fontFamily: "Cairo",
                                                color: Colors.black87,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ],
                              if (content.reference != null) ...[
                                const SizedBox(height: 24),
                                Container(
                                  padding: const EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    color:
                                        AppColors.lightPrimary.withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(
                                      color: AppColors.lightPrimary,
                                      width: 1,
                                    ),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                        'المصدر',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "Cairo",
                                          color: AppColors.darkPrimary,
                                        ),
                                      ),
                                      const SizedBox(height: 8),
                                      Text(
                                        content.reference!,
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontFamily: "Cairo",
                                          color: Colors.black87,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
            return const Center(
              child: CircularProgressIndicator(
                valueColor:
                    AlwaysStoppedAnimation<Color>(AppColors.lightPrimary),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildInfoCard({
    required IconData icon,
    required String title,
    required String content,
  }) {
    return Container(
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: AppColors.lightPrimary.withOpacity(0.1),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        children: [
          Icon(icon, color: AppColors.lightPrimary),
          const SizedBox(width: 12),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  fontFamily: "Cairo",
                  color: AppColors.darkPrimary,
                ),
              ),
              Text(
                content,
                style: const TextStyle(
                  fontSize: 16,
                  fontFamily: "Cairo",
                  color: Colors.black87,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
